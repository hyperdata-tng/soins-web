import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/material.dart';
import 'package:web/views/skins/layout/content.dart';
import 'package:web/views/skins/layout/header.dart';
import 'package:web/views/skins/layout/sidebar.dart';
import 'package:web/views/skins/widgets/breadcrumbs.dart';

class Wrapper extends StatelessWidget {

  const Wrapper({
    Key? key,
    this.title,
    this.subTitle,
    this.menuKey = const [],
    this.child,
    this.paddingContent,
    this.breadcrumbs = const [],
  }) : super(key: key);

  final String? title;

  final String? subTitle;

  final Widget? child;

  final EdgeInsetsGeometry? paddingContent;

  final List<String> menuKey;

  final List<Breadcrumb> breadcrumbs;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: BreakPoint.isMobile(context) ? SkinSidebar(menuKey: menuKey,shadow: false) : null,
      body: SafeArea(
        child: Container(
          child: Row(
            children: [
              BreakPoint.isDesktop(context) || BreakPoint.isTablet(context)
                  ? SkinSidebar(menuKey: menuKey) : Container(),
              Expanded(child: Container(
                child: Column(
                  verticalDirection: VerticalDirection.up,
                  children: [
                    SkinContent(
                      title: title,
                      subTitle: subTitle,
                      child: child,
                      padding: paddingContent,
                      breadcrumbs: breadcrumbs,
                    ),
                    SkinHeader(),
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }
}