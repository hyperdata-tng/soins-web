import 'package:flutter/material.dart';
import 'package:web/views/skins/layout/header-left.dart';
import 'package:web/views/skins/layout/header-right.dart';

class SkinHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Color(0xffd9d9d9),
            spreadRadius: 2.0,
            blurRadius: 10.0,
            offset: Offset(12.0, 0.0)
          )
        ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(child: HeaderLeftSide()),
          HeaderRightSide(),
        ],
      ),
    );
  }
}