import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/material.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/userdt_model.dart';
import 'package:web/utils/session.dart';

class SidebarProfile extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _SidebarProfileState();
  }
}

class _SidebarProfileState extends State<SidebarProfile> {

  SessionUtils _session = SessionUtils(
    userdetail: UserDetailModel(),
  );

  @override
  void initState() {
    main();
    super.initState();
  }

  void updateState(VoidCallback function) {
    if(mounted)
      setState(() {
        function();
      });
  }

  void main() {
    SessionUtils.getInstance().then((session) {
      updateState(() {
        _session = session;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget widget = screenDesktop();

    if(BreakPoint.isTablet(context))
      widget = screenTablet();

    return widget;
  }

  Widget screenDesktop() {
    return Container(
      margin: EdgeInsets.only(bottom: 20.0),
      child: Column(
        children: [
          Container(
            width: 80,
            height: 80,
            margin: EdgeInsets.only(bottom: 10.0),
            child: CircleAvatar(
              backgroundColor: Colors.grey.withOpacity(0.5),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 5.0),
            child: Text(notNull(_session.fullname, nullValue: '-'), style: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
            )),
          ),
          Text(notNull(_session.userdetail.usertype.typename, nullValue: '-'), style: TextStyle(
            fontSize: 12.0,
            color: Colors.grey,
            fontWeight: FontWeight.w100,
          ))
        ],
      ),
    );
  }

  Widget screenTablet() {
    return Container(
      margin: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 20.0),
      child: Column(
        children: [
          Container(
            width: 50,
            height: 50,
            child: CircleAvatar(
              backgroundColor: Colors.grey.withOpacity(0.5),
            ),
          ),
        ],
      ),
    );
  }
}