import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/material.dart';
import 'package:web/routes.dart';
import 'package:web/routes/home_route.dart';
import 'package:web/utils/utils.dart';
import 'package:web/views/skins/layout/sidebar-menus.dart';
import 'package:web/views/skins/layout/sidebar-profile.dart';

class SkinSidebar extends StatefulWidget {

  const SkinSidebar({
    Key? key,
    this.menuKey = const [],
    this.shadow = true,
  }) : super(key: key);

  final bool shadow;

  final List<String> menuKey;

  @override
  State<StatefulWidget> createState() {
    return _SkinSidebarState();
  }
}

class _SkinSidebarState extends State<SkinSidebar> {
  @override
  Widget build(BuildContext context) {

    Widget widget = screenDesktop();
    if(BreakPoint.isTablet(context))
      widget = screenTable();

    return widget;
  }

  Widget screenDesktop() {
    return Container(
      width: 250.0,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: !widget.shadow ? [] : [Styles.shadowRegular]
      ),
      child: Column(
        children: [
          Container(
            color: Colors.white,
            margin: EdgeInsets.only(bottom: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () => Routes.redirect(context, HomeRoute.home),
                    child: Container(
                      padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                      child: Text('ADMIN TEMPLATE',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0
                        ),
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                      )
                    ),
                    hoverColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    focusColor: Colors.transparent,
                  ),
                )),
              ],
            ),
          ),
          SidebarProfile(),
          Expanded(child: Scrollbar(
            child: SingleChildScrollView(
              child: SidebarMenus(menuKey: widget.menuKey),
            ),
          )),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text('Version 1.0.0', style: TextStyle(
              color: Colors.black.withOpacity(0.8),
              fontSize: 12.0,
            )),
          )
        ],
      ),
    );
  }

  Widget screenTable() {
    return Container(
      width: 80.0,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: !widget.shadow ? [] : [Styles.shadowRegular]
      ),
      child: Column(
        children: [
          Container(
            child: Row(
              children: [
                Expanded(child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () => Routes.redirect(context, HomeRoute.home),
                    child: Container(
                      padding: EdgeInsets.fromLTRB(15.0, 20.0, 15.0, 20.0),
                      child: Text('ADMIN TEMPLATE',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0
                        ),
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                      )
                    ),
                    hoverColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    focusColor: Colors.transparent,
                  ),
                )),
              ],
            ),
          ),
          SidebarProfile(),
          Expanded(child: Scrollbar(
            child: SingleChildScrollView(
              child: SidebarMenus(menuKey: widget.menuKey),
            ),
          ))
        ],
      ),
    );
  }
}