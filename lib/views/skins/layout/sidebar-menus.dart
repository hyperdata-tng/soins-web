import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/models/masters/userdt_model.dart';
import 'package:web/routes.dart';
import 'package:web/services/masters/menu_service.dart';
import 'package:web/utils/session.dart';
import 'package:web/views/skins/layout/sidebar-menus-child.dart';
import 'package:web/views/skins/layout/sidebar-menus-item.dart';

class SidebarMenus extends StatefulWidget {

  const SidebarMenus({
    Key? key,
    this.menuKey = const [],
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SidebarMenusState();
  }

  final List<String> menuKey;
}

class _SidebarMenusState extends State<SidebarMenus> {

  bool _loaded = false;
  SessionUtils _session = SessionUtils(
    userdetail: UserDetailModel()
  );

  @override
  void initState() {
    main();
    super.initState();
  }

  void updateState(VoidCallback function) {
    if(mounted)
      setState(() {
        function();
      });
  }

  void main() {
    SessionUtils.getInstance().then((session) => updateState(() {
      _session = session;

      if(!_loaded && _session.accessmenus.length == 0)
        fetchMenu();

      else _loaded = true;
    }));
  }

  void fetchMenu() {
    MenuService service = MenuService();
    service.access(_session.userdetail.usertype.typeid!, _session.userdetail.businesspartner.bpid!).then((res) {
      if(res.result!) {
        SessionUtils.setAccessMenus(List<MenuModel>
            .from(res.data.map((e) => MenuModel.fromJson(e))));
        main();
      }

      _loaded = true;
    });
  }

  List<MenuModel> _getParentId(int parentid) {
    List<MenuModel> menus = List<MenuModel>.from([]);
    _session.accessmenus.forEach((accessmenu) {
      if(accessmenu.masterid == parentid)
        menus.add(accessmenu);
    });
    menus.sort((MenuModel a, MenuModel b) {
      return a.seq! > b.seq! ? 1 : 0;
    });
    return menus;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scrollbar(
        child: SingleChildScrollView(
          child: Column(
            children: _getParentId(0).map((accessmenu) {

              List<MenuModel> childrens = _getParentId(accessmenu.menuid!);

              return SidebarMenuItem(
                active: widget.menuKey.contains(accessmenu.route),
                menuKey: accessmenu.route!,
                icon: Icons.dashboard,
                label: accessmenu.menunm!,
                onPressed: () => childrens.length == 0 ? Routes.redirect(context, accessmenu.route!) : {},
                children: childrens.map((children) {
                  return menuChild(children);
                }).toList(),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }

  SidebarMenuChild menuChild(MenuModel accessmenu) {
    List<MenuModel> childrens = _getParentId(accessmenu.menuid!);

    return SidebarMenuChild(
      active: widget.menuKey.contains(accessmenu.route),
      label: accessmenu.menunm!,
      menuKey: accessmenu.route!,
      onPressed: () => childrens.length == 0 ? Routes.redirect(context, accessmenu.route!) : {},
      children: childrens.map((children) {
        return menuChild(children);
      }).toList(),
    );
  }
}