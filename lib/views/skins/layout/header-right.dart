import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/helpers/widget/confirm_dialog.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/models/masters/user_model.dart';
import 'package:web/models/masters/userdt_model.dart';
import 'package:web/routes.dart';
import 'package:web/routes/auth/auth_route.dart';
import 'package:web/services/app_service.dart';
import 'package:web/utils/session.dart';
import 'package:web/utils/src/hover_decoration.dart';
import 'package:web/utils/utils.dart';
import 'package:web/views/skins/widgets/rounded_button.dart';

class HeaderRightSide extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HeaderRightSideState();
  }
}

class _HeaderRightSideState extends State<HeaderRightSide> {

  SessionUtils _session = SessionUtils(
    userdetail: UserDetailModel()
  );

  @override
  void initState() {
    main();
    super.initState();
  }

  void updateState(VoidCallback function) {
    if(mounted) {
      setState(() {
        function();
      });
    }
  }

  void main() {
    SessionUtils.getInstance().then((session) => updateState(() {
      _session = session;
    }));
  }

  Future _update() async {
    SessionUtils session = await SessionUtils.getInstance();
    AppService().update(
      notNull(session.userid),
      notNull(session.userdetail.bpid),
      notNull(session.userdetail.usertypeid),
    ).then((res) {
      if(res.result!) {
        List<MenuModel> accessmenus = List<MenuModel>.from(res.data['accessmenus'].map((e) {
          return MenuModel.fromJson(e);
        }));

        UserModel user = UserModel.fromJson(res.data['user']);
        SessionUtils.setSession(SessionUtils(
          userid: user.userid,
          fullname: user.userfullname,
          token: user.token,
          expiredtoken: parseDate(res.data['user']['expired']),
          userdetail: user.userdetail,
          userdetails: user.userdetails,
        ));
        SessionUtils.setAccessMenus(accessmenus);

        setState(() {
          Routes.redirect(context, ModalRoute.of(context)!.settings.name.toString());
        });
      } else {
        showDialog(
          context: context,
          builder: (context) => ConfirmDialog(
            title: "Informasi",
            message: "Terdapat anomali kegiatan, agar aplikasi dapat berjalan denga baik lakukan login ulang",
            buttons: [ConfirmDialogButton.POSITION_BUTTON],
            positiveButton: BsButton(
              label: Text("OK"),
              style: BsButtonStyle.primary,
              size: BsButtonSize.btnSm,
              onPressed: () {
                SessionUtils.removeSession().then((value) {
                  Routes.redirect(context, AuthRoute.login);
                });
              },
            ),
          )
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(0.0, 0.0, 12.0, 0.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              BsButton(
                margin: EdgeInsets.only(right: 5.0),
                label: Text('Update'),
                prefixIcon: Icons.cloud_upload_rounded,
                style: Styles.roundedPrimary,
                onPressed: () => _update(),
              ),
              BsDropdownButton(
                toggleMenu: (_) {
                  return RoundedButton(
                    icon: Icons.more_vert_rounded,
                    backgroundColor: Colors.white,
                    color: Colors.black,
                    hoverDecoration: HoverDecoration.blueAccent,
                    onPressed: () => _.toggle(),
                  );
                },
                dropdownMenuStyle: BsDropdownMenuStyle(
                  borderRadius: BorderRadius.circular(2.0)
                ),
                dropdownMenu: BsDropdownMenu(
                  children: [
                    BsDropdownItem(
                      padding: EdgeInsets.fromLTRB(15.0, 12.0, 15.0, 12.0),
                      child: Row(
                        children: [
                          Icon(Icons.person_rounded, size: 16.0),
                          Container(
                            margin: EdgeInsets.only(left: 10.0),
                            child: Text('Profil'),
                          ),
                        ],
                      ),
                      onPressed: () async {
                        await SessionUtils.removeSession();
                        Routes.redirect(context, AuthRoute.login);
                      }
                    ),
                    BsDropdownItem(
                      padding: EdgeInsets.fromLTRB(15.0, 12.0, 15.0, 12.0),
                      child: Row(
                        children: [
                          Icon(Icons.settings_power_outlined, size: 16.0),
                          Container(
                            margin: EdgeInsets.only(left: 10.0),
                            child: Text('Logout'),
                          ),
                        ],
                      ),
                      onPressed: () async {
                        await SessionUtils.removeSession();
                        Routes.redirect(context, AuthRoute.login);
                      }
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}