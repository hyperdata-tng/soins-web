import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/material.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/userdt_model.dart';
import 'package:web/routes.dart';
import 'package:web/routes/home_route.dart';
import 'package:web/utils/session.dart';

class HeaderLeftSide extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HeaderLeftSideState();
  }
}

class _HeaderLeftSideState extends State<HeaderLeftSide> {

  SessionUtils _session = SessionUtils(
    userdetail: UserDetailModel()
  );

  @override
  void initState() {
    main();
    super.initState();
  }

  void updateState(VoidCallback function) {
    if(mounted)
      setState(() {
        function();
      });
  }

  void main() async {
    SessionUtils.getInstance().then((session) {
      updateState(() {
        _session = session;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              BreakPoint.isDesktop(context)
                  || BreakPoint.isTablet(context) ? Container() : Container(
                padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                child: Material(
                  child: InkWell(
                    child: Container(
                      child: Icon(Icons.format_align_justify_rounded,
                        color: Colors.black,
                        size: 24.0,
                      ),
                    ),
                    onTap: () => Scaffold.of(context).openDrawer(),
                    highlightColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                  ),
                ),
              ),
              BsDropdownButton(
                  toggleMenu: (_) => Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () => _.toggle(),
                      child: Container(
                        padding: EdgeInsets.fromLTRB(15.0, 14.0, 15.0, 14.0),
                        decoration: BoxDecoration(),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 10.0),
                              child: Icon(Icons.home_work_outlined, size: 26.0,),
                            ),
                            Container(
                              margin: EdgeInsets.only(right: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.0),
                                    child: Text(notNull(_session.userdetail.businesspartner.bpname, nullValue: '-'),
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    )
                                  ),
                                  Container(
                                    child: Text(notNull(_session.userdetail.usertype.typename, nullValue: '-'),
                                      style: TextStyle(
                                        fontSize: 12.0
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    )
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  dropdownMenuStyle: BsDropdownMenuStyle(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                  dropdownMenu: BsDropdownMenu(
                    children: _session.userdetails.map((userdetail) {
                      return BsDropdownItem(
                        padding: EdgeInsets.all(10.0),
                        onPressed: () => updateState(() async {
                          if(userdetail.userdtid != _session.userdetail.userdtid) {
                            SessionUtils.updateUserDetail(userdetail);
                            SessionUtils.remove(SessionCode.accessmenus);
                            Routes.redirect(context, HomeRoute.home);
                          }
                        }),
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(bottom: 2.0),
                                child: Text(notNull(userdetail.businesspartner.bpname, nullValue: '-'),
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                )
                              ),
                              Container(
                                child: Text(notNull(userdetail.usertype.typename, nullValue: '-'),
                                  style: TextStyle(
                                    fontSize: 12.0
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                )
                              )
                            ],
                          ),
                        ),
                      );
                    }).toList(),
                  )
              ),
            ],
          )
        ],
      ),
    );
  }
}