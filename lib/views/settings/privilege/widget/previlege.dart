import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:web/constants/constants.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/models/masters/role_model.dart';
import 'package:web/presenters/settings/previlege_presenter.dart';
import 'package:web/routes/settings/previlege_route.dart';
import 'package:web/routes/settings/previleges_route.dart';
import 'package:web/utils/utils.dart';
import 'package:web/views/settings/privilege/source/datasource.dart';
import 'package:web/views/skins/widgets/breadcrumbs.dart';
import 'package:web/views/skins/widgets/wrapper.dart';

class PrevilegeView extends StatefulWidget {

  const PrevilegeView({
    Key? key,
    this.developer = false,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PrevilegeViewState();
  }

  final bool developer;
}

class _PrevilegeViewState extends State<PrevilegeView> implements PrevilegePresenterContract {

  GlobalKey<FormState> _formState = GlobalKey<FormState>();

  int _indexRow = 0;
  bool hasAccessUpdate = false;

  late PrevilegePresenter _presenter;
  late PrevilegeForm _form;

  @override
  void initState() {
    _presenter = PrevilegePresenter(this, widget.developer);
    _form = PrevilegeForm(context: context, presenter: _presenter);
    hasAccess(widget.developer ? PrevilegesRoute.routeKey : PrevilegeRoute.routeKey, DBTypes.menuAccessUpdate).then((value) {
      setState(() {
        hasAccessUpdate = value;
      });
    });
    super.initState();
  }

  @override
  void updateState() {
    _indexRow = 0;
    if(mounted)
      setState(() {
      });
  }

  void _onChecked(bool value, RoleModel access, MenuModel menu) {

    bool hasAccess = false;
    MenuModel mapmenu = _presenter.menus[menu.menuid!]!;
    List<RoleModel> roles = List<RoleModel>.from([]);

    /// Set checkbox yang diklik
    for(int i = 0; i < mapmenu.accesses.length; i++) {
      RoleModel macc = mapmenu.accesses[i];

      /// Jika diklik pada checkbox view maka semua chekbox akses true
      if(access.access.typecd == DBTypes.menuAccessView) {
        macc.checked = value;
      }

      /// Jika tidak maka yang true hanya yang diklik
      else if(access.access.typecd == macc.access.typecd) {
        macc.checked = value;
      }

      roles.add(macc);
    }

    /// Check jika ada checkbox yang true atau tidak di menu
    for(int i = 0; i < roles.length; i++) {
      RoleModel macc = roles[i];

      /// Jika ada salah satu maka for akan berhenti, karena sudah
      /// menunjukan jika pada menu terdapat checkbox yang true
      if(macc.access.typecd != DBTypes.menuAccessView) {
        if(macc.checked!) {
          hasAccess = true;
          break;
        }
      }
    }

    /// Set checkbox view to true jika ada checkbox yang true di menu
    /// jika tidak ada maka akan set checkbox to false
    for(int i = 0; i < roles.length; i++) {
      RoleModel macc = roles[i];

      /// Set chexkbo view
      if(macc.access.typecd == DBTypes.menuAccessView) {
        if(hasAccess && !macc.checked!)
          macc.checked = hasAccess;
      }
    }

    mapmenu.accesses = RoleModel.jsonList(roles, jsonFormat: false);

    /// Check parent akses
    if(menu.masterid != null && menu.masterid != 0)
      _setParentChecked(value, menu);

    /// Check child akses
    if(!value)
      _setChildChecked(menu);

    updateState();
  }

  void _setParentChecked(bool value, MenuModel menu) {
    MenuModel mapmenu = _presenter.menus[menu.masterid!]!;
    List<MenuModel> children = _getParentId(mapmenu.menuid!);

    /// Check children dari parent menu yang diklik
    /// dari menu ada checkbox yang true atau tidak
    bool hasAccess = false;
    for(int i = 0; i < children.length; i++) {
      MenuModel child = children[i];
      for(int j= 0; j < child.accesses.length; j++) {
        RoleModel macc = child.accesses[j];
        if(macc.access.typecd == DBTypes.menuAccessView) {
          if(macc.checked!) {
            hasAccess = true;
            break;
          }
        }
      }

      if(hasAccess)
        break;
    }

    /// Jika dari checkbox yang diklik terdapat parent yang
    /// belum dapat akses view, maka akan otomatis set checkbox view
    /// ke true
    List<RoleModel> roles = List<RoleModel>.from([]);
    for(int i = 0; i < mapmenu.accesses.length; i++) {
      RoleModel macc = mapmenu.accesses[i];
      if(macc.access.typecd == DBTypes.menuAccessView) {
        macc.checked = hasAccess;
      }

      roles.add(macc);
    }

    mapmenu.accesses = RoleModel.jsonList(roles, jsonFormat: false);

    /// Nested check parent
    if (mapmenu.masterid != null && mapmenu.masterid != 0) {
      _setParentChecked(value, mapmenu);
    }
  }

  void _setChildChecked(MenuModel menu) {
    List<MenuModel> children = _getParentId(menu.menuid!);

    /// Set chekbox sesuai parent dari children tersebut
    for(int i = 0; i < children.length; i++) {
      MenuModel child = children[i];
      List<RoleModel> roles = List<RoleModel>.from([]);
      for(int j = 0; j < child.accesses.length; j++) {
        RoleModel macc = child.accesses[j];
        macc.checked = false;
        roles.add(macc);
      }

      child.accesses = RoleModel.jsonList(roles, jsonFormat: false);

      /// Nested check children
      if(_getParentId(child.menuid!).length > 0)
        _setChildChecked(child);
    }
  }

  List<MenuModel> _getParentId(int parentid) {
    List<MenuModel> menus = List<MenuModel>.from([]);
    _presenter.menus.forEach((key, value) {
      if(value.masterid == parentid)
        menus.add(value);
    });
    menus.sort((MenuModel a, MenuModel b) {
      return a.seq! > b.seq! ? 1 : 0;
    });
    return menus;
  }

  @override
  Widget build(BuildContext context) {

    return Wrapper(
      title: PrevilegeText.title,
      subTitle: PrevilegeText.subTitle,
      menuKey: [DBRoute.settingRoute, widget.developer ? PrevilegesRoute.previleges : PrevilegeRoute.previlege],
      paddingContent: EdgeInsets.zero,
      breadcrumbs: [
        Breadcrumbs.home(context),
        Breadcrumb(label: DBText.master),
        Breadcrumb(label: PrevilegeText.title)
      ],
      child: Container(
        child: Column(
          children: [
            Form(
              key: _formState,
              child: BsRow(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                margin: EdgeInsets.only(bottom: 15.0),
                gutter: EdgeInsets.only(left: 5.0, right: 5.0),
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  !widget.developer ? BsCol(sizes: ColScreen.all(Col(width: 0))) : BsCol(
                    sizes: ColScreen.inDevice(mobile: Col.col_12, tablet: Col.col_4, desktop: Col.col_3),
                    child: _form.selectBusinessPartner(),
                  ),
                  BsCol(
                    sizes: ColScreen.inDevice(mobile: Col.col_12, tablet: Col.col_4, desktop: Col.col_3),
                    child: _form.selectUserType(),
                  ),
                  BsCol(
                    sizes: ColScreen.inDevice(mobile: Col.col_12, tablet: Col.col_4, desktop: Col.col_3),
                    child: _form.selectMenuType(),
                  ),
                  BsCol(
                    sizes: ColScreen.inDevice(mobile: Col.col_12, tablet: Col.col_4, desktop: Col.col_3),
                    child: Row(
                      children: [
                        BsButton(
                          margin: EdgeInsets.only(top: 10.0),
                          label: Text('Tampilkan'),
                          style: BsButtonStyle.primary,
                          onPressed: () {
                            if(_formState.currentState!.validate() && !_presenter.isLoading)
                              _presenter.load(context);
                          },
                        ),
                        !hasAccessUpdate || _presenter.menus.length == 0 ? Container() : BsButton(
                          margin: EdgeInsets.only(left: 5.0, top: 10.0),
                          label: Text('Simpan'),
                          style: BsButtonStyle.primary,
                          onPressed: () => _presenter.save(context),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: Styles.paddingContent,
              child: BsCard(
                style: Styles.boxCard,
                children: [
                  BsCardContainer(title: Text(DBText.dataTitle(PrevilegeText.title))),
                  BsCardContainer(
                    child: Row(
                      children: [
                        Expanded(child: _tableAccess())
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _tableAccess() {
    List<TableRow> rows = List<TableRow>.from([]);

    rows.add(TableRow(
      decoration: BoxDecoration(
        color: Color(0xfff1f1f1),
      ),
      children: [
        Container(
          padding: EdgeInsets.all(15.0),
          child: Text('Menu')
        ),
        Container(
          padding: EdgeInsets.all(15.0),
          child: Row(
            children: _presenter.accesses.map((access) {
              return Container(
                width: 150,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(notNull(access.typename),
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                      )
                    )
                  ],
                ),
              );
            }).toList(),
          ),
        )
      ]
    ));

    _getParentId(0).forEach((menu) {
      List<Widget> children = List<Widget>.from([]);
      menu.accesses.forEach((macc) {
        children.add(Container(
          width: 150,
          child: Align(
            child: Checkbox(
              value: macc.checked,
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              onChanged: (value) => _onChecked(value!, macc, menu),
            ),
          ),
        ));
      });

      rows.add(TableRow(
        decoration: BoxDecoration(
          color: _indexRow % 2 == 0 ? Colors.transparent : Color(0xfffafafa),
        ),
        children: [
          Container(
            padding: EdgeInsets.all(8.0),
            child: Text(notNull(menu.menunm),
              style: TextStyle(
                fontWeight: _getParentId(menu.menuid!).length > 0 ? FontWeight.bold : FontWeight.normal,
                fontSize: 14.0
              ),
            )
          ),
          Row(children: children)
        ]
      ));

      _indexRow++;

      _tableChild(rows, menu, 0);
    });

    return LayoutBuilder(
      builder: (context, constraints) {
        return Scrollbar(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Column(
              children: [
                ConstrainedBox(
                  constraints: BoxConstraints(minWidth: constraints.maxWidth),
                  child: Table(
                    columnWidths: {
                      0: FixedColumnWidth(200),
                      1: FixedColumnWidth(150.0*_presenter.accesses.length +50)
                    },
                    children: rows,
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  void _tableChild(List<TableRow> rows, MenuModel menu, int nested) {
    nested++;

    _getParentId(menu.menuid!).forEach((child) {
      List<Widget> children = List<Widget>.from([]);
      child.accesses.forEach((access) {
        children.add(Container(
          width: 150,
          child: Align(
            child: Checkbox(
              value: access.checked,
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              onChanged: (value) => _onChecked(value!, access, child),
            ),
          ),
        ));
      });

      rows.add(TableRow(
        decoration: BoxDecoration(
          color: _indexRow % 2 == 0 ? Colors.transparent : Color(0xfffafafa),
        ),
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(8.0 + (15.0 * nested), 8.0, 8.0, 8.0),
            child: Text(notNull(child.menunm),
              style: TextStyle(
                fontWeight: _getParentId(child.menuid!).length > 0 ? FontWeight.bold : FontWeight.normal,
                fontSize: 14.0
              ),
            )
          ),
          Row(children: children)
        ]
      ));

      _indexRow++;

      _tableChild(rows, child, nested);
    });
  }

}