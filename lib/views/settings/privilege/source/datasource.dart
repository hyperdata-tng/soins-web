library datasource;

import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:web/constants/constants.dart';
import 'package:web/helpers/select_api.dart';
import 'package:web/presenters/settings/previlege_presenter.dart';
import 'package:web/utils/utils.dart';

part 'previlegetext.dart';
part 'previlegesource.dart';
part 'formsource.dart';