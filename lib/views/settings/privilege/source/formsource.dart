part of datasource;

abstract class PrevilegeFormSource {
  bool isLoading = false;

  BsSelectBoxController selectUserType = BsSelectBoxController();
  BsSelectBoxController selectMenuType = BsSelectBoxController();
  BsSelectBoxController selectBp = BsSelectBoxController();
}

class PrevilegeForm {

  final BuildContext context;

  final PrevilegePresenter presenter;

  PrevilegeForm({
    required this.context,
    required this.presenter,
  });

  Widget selectBusinessPartner() {
    return BsFormGroup(
      label: Text(PrevilegeText.formBusinessPartner),
      child: BsSelectBox(
        searchable: true,
        disabled: presenter.isLoading,
        hintText: DBText.placeholderSelect(PrevilegeText.formBusinessPartner),
        selectBoxController: presenter.selectBp,
        validators: [
          UtilsValidation.selectRequired(PrevilegeText.formBusinessPartner)
        ],
        serverSide: (params) => selectApiBusinessPartner(params),
      ),
    );
  }

  Widget selectUserType() {
    return BsFormGroup(
      label: Text(PrevilegeText.formUserType),
      child: BsSelectBox(
        searchable: true,
        disabled: presenter.isLoading,
        hintText: DBText.placeholderSelect(PrevilegeText.formUserType),
        selectBoxController: presenter.selectUserType,
        validators: [
          UtilsValidation.selectRequired(PrevilegeText.formUserType)
        ],
        serverSide: (params) => selectApiUserType(params),
      ),
    );
  }

  Widget selectMenuType() {
    return BsFormGroup(
      label: Text(PrevilegeText.formMenuType),
      child: BsSelectBox(
        searchable: true,
        disabled: presenter.isLoading,
        hintText: DBText.placeholderSelect(PrevilegeText.formMenuType),
        selectBoxController: presenter.selectMenuType,
        validators: [
          UtilsValidation.selectRequired(PrevilegeText.formMenuType)
        ],
        serverSide: (params) => selectApiTypeMenu(params),
      ),
    );
  }
}