part of datasource;

class PrevilegeText {

  static const String title = 'Hak Akses';
  static const String subTitle = 'Semua data hak akses';

  static const String formMenu = 'Menu';
  static const String formUserType = 'Grup Pengguna';
  static const String formMenuType = 'Menu';
  static const String formBusinessPartner = 'Bisnis Partner';
}