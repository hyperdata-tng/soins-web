import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:web/constants/constants.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/presenters/masters/bpmenu_presenter.dart';
import 'package:web/routes/masters/bpmenu_route.dart';
import 'package:web/utils/utils.dart';
import 'package:web/views/masters/businesspartner/menus/source/datasource.dart';
import 'package:web/views/skins/widgets/breadcrumbs.dart';
import 'package:web/views/skins/widgets/wrapper.dart';

class BpMenuView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BpMenuViewState();
  }
}

class _BpMenuViewState extends State<BpMenuView> implements BpMenuPresenterContract {

  GlobalKey<FormState> _formState = GlobalKey<FormState>();

  late BpMenuForm _form;
  late BpMenuPresenter _presenter;

  int _indexRow = 0;
  bool hasAccessUpdate = false;

  @override
  void initState() {
    _presenter = BpMenuPresenter(this);
    _form = BpMenuForm(context: context, presenter: _presenter);
    hasAccess(BpMenuRoute.routeKey, DBTypes.menuAccessUpdate).then((value) {
      setState(() {
        hasAccessUpdate = value;
      });
    });
    super.initState();
  }

  @override
  void updateState() {
    _indexRow = 0;
    setState(() {
    });
  }

  void _onChecked(bool value, MenuModel menu) {
    menu.checked = value;

    if(menu.masterid != null && menu.masterid != 0)
      _setParentChecked(menu);

    if(!value && _getParentId(menu.menuid!).length > 0)
      _setChildrenChecked(menu);
    
    updateState();
  }

  void _setParentChecked(MenuModel menu) {
    MenuModel mapmenu = _presenter.menus[menu.masterid!]!;
    List<MenuModel> children = _getParentId(mapmenu.menuid!);
    print(children.length);

    bool hasAccess = false;
    for(int i = 0; i < children.length; i++) {
      MenuModel child = children[i];
      if(child.checked!) {
        hasAccess = true;
        break;
      }
    }

    mapmenu.checked = hasAccess;

    if(mapmenu.masterid != null && mapmenu.masterid != 0)
      _setParentChecked(mapmenu);
  }

  void _setChildrenChecked(MenuModel menu) {
    List<MenuModel> children = _getParentId(menu.menuid!);
    for(int i = 0; i < children.length; i++) {
      MenuModel child = children[i];
      child.checked = false;

      if(_getParentId(child.menuid!).length > 0)
        _setChildrenChecked(child);
    }
  }

  List<MenuModel> _getParentId(int parentid) {
    List<MenuModel> menus = List<MenuModel>.from([]);
    _presenter.menus.forEach((key, value) {
      if(value.masterid == parentid)
        menus.add(value);
    });
    menus.sort((MenuModel a, MenuModel b) {
      return a.seq! > b.seq! ? 1 : 0;
    });
    return menus;
  }

  @override
  Widget build(BuildContext context) {
    return Wrapper(
      title: BpMenuText.title,
      subTitle: BpMenuText.subTitle,
      paddingContent: EdgeInsets.zero,
      menuKey: [DBRoute.masterRoute, DBRoute.businesspartnerRoute, BpMenuRoute.bpmenu],
      breadcrumbs: [
        Breadcrumbs.home(context),
        Breadcrumb(label: DBText.master),
        Breadcrumb(label: DBText.businesspartner),
        Breadcrumb(label: BpMenuText.title)
      ],
      child: Container(
        child: Column(
          children: [
            Form(
              key: _formState,
              child: BsRow(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                margin: EdgeInsets.only(bottom: 15.0),
                gutter: EdgeInsets.only(left: 5.0, right: 5.0),
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  BsCol(
                    sizes: ColScreen.inDevice(mobile: Col.col_12, tablet: Col.col_4, desktop: Col.col_3),
                    child: _form.selectBusinessPartner(),
                  ),
                  BsCol(
                    sizes: ColScreen.inDevice(mobile: Col.col_12, tablet: Col.col_4, desktop: Col.col_3),
                    child: _form.selectMenuType(),
                  ),
                  BsCol(
                    sizes: ColScreen.inDevice(mobile: Col.col_12, tablet: Col.col_4, desktop: Col.col_3),
                    child: Row(
                      children: [
                        BsButton(
                          margin: EdgeInsets.only(top: 10.0),
                          label: Text('Tampilkan'),
                          style: BsButtonStyle.primary,
                          onPressed: () {
                            if(_formState.currentState!.validate() && !_presenter.isLoading)
                              _presenter.load(context);
                          },
                        ),
                        !hasAccessUpdate || _presenter.menus.length == 0 ? Container() : BsButton(
                          margin: EdgeInsets.only(left: 5.0, top: 10.0),
                          label: Text('Simpan'),
                          style: BsButtonStyle.primary,
                          onPressed: () => _presenter.save(context),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: Styles.paddingContent,
              child: BsCard(
                style: Styles.boxCard,
                children: [
                  BsCardContainer(title: Text(DBText.dataTitle(BpMenuText.title))),
                  BsCardContainer(
                    child: Row(
                      children: [
                        Expanded(child: _tableAccess())
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _tableAccess() {
    List<TableRow> rows = List<TableRow>.from([]);

    rows.add(TableRow(
        decoration: BoxDecoration(
          color: Color(0xfff1f1f1),
        ),
        children: [
          Container(
              padding: EdgeInsets.all(15.0),
              child: Text('Menu')
          ),
          Container(
            padding: EdgeInsets.all(15.0),
            child: Container(
              width: 150,
              child: Row(
                children: [
                  Expanded(
                    child: Text('Akses')
                  )
                ],
              ),
            ),
          )
        ]
    ));

    _getParentId(0).forEach((menu) {
      List<Widget> children = List<Widget>.from([]);
      children.add(Container(
        width: 150,
        child: Align(
          child: Checkbox(
            value: menu.checked,
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            onChanged: (value) => _onChecked(value!, menu),
          ),
        ),
      ));

      rows.add(TableRow(
        decoration: BoxDecoration(
          color: _indexRow % 2 == 0 ? Colors.transparent : Color(0xfffafafa),
        ),
        children: [
          Container(
            padding: EdgeInsets.all(8.0),
            child: Text(notNull(menu.menunm),
              style: TextStyle(
                fontWeight: _getParentId(menu.menuid!).length > 0 ? FontWeight.bold : FontWeight.normal,
                fontSize: 14.0
              ),
            )
          ),
          Row(children: children)
        ]
      ));

      _indexRow++;

      _tableChild(rows, menu, 0);
    });

    return LayoutBuilder(
      builder: (context, constraints) {
        return Scrollbar(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Column(
              children: [
                ConstrainedBox(
                  constraints: BoxConstraints(minWidth: constraints.maxWidth),
                  child: Table(
                    columnWidths: {
                      0: FixedColumnWidth(200),
                      1: FixedColumnWidth(150)
                    },
                    children: rows,
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  void _tableChild(List<TableRow> rows, MenuModel menu, int nested) {
    nested++;

    _getParentId(menu.menuid!).forEach((child) {
      List<Widget> children = List<Widget>.from([]);
      children.add(Container(
        width: 150,
        child: Align(
          child: Checkbox(
            value: child.checked,
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            onChanged: (value) => _onChecked(value!, child),
          ),
        ),
      ));

      rows.add(TableRow(
        decoration: BoxDecoration(
          color: _indexRow % 2 == 0 ? Colors.transparent : Color(0xfffafafa),
        ),
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(8.0 + (15.0 * nested), 8.0, 8.0, 8.0),
            child: Text(notNull(child.menunm),
              style: TextStyle(
                fontWeight: _getParentId(child.menuid!).length > 0 ? FontWeight.bold : FontWeight.normal,
                fontSize: 14.0
              ),
            )
          ),
          Row(children: children)
        ]
      ));

      _indexRow++;

      _tableChild(rows, child, nested);
    });
  }
}