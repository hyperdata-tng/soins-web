part of datasource;

abstract class BpMenuFormSource {
  bool isLoading = false;

  BsSelectBoxController selectMenuType = BsSelectBoxController();
  BsSelectBoxController selectBp = BsSelectBoxController();
}

class BpMenuForm {

  final BuildContext context;

  final BpMenuPresenter presenter;

  const BpMenuForm({
    required this.context,
    required this.presenter,
  });

  Widget selectBusinessPartner() {
    return BsFormGroup(
      label: Text(BpMenuText.formBusinessPartner),
      child: BsSelectBox(
        searchable: true,
        disabled: presenter.isLoading,
        hintText: DBText.placeholderSelect(BpMenuText.formBusinessPartner),
        selectBoxController: presenter.selectBp,
        validators: [
          UtilsValidation.selectRequired(BpMenuText.formBusinessPartner)
        ],
        serverSide: (params) => selectApiBusinessPartner(params),
      ),
    );
  }

  Widget selectMenuType() {
    return BsFormGroup(
      label: Text(BpMenuText.formMenuType),
      child: BsSelectBox(
        searchable: true,
        disabled: presenter.isLoading,
        hintText: DBText.placeholderSelect(BpMenuText.formMenuType),
        selectBoxController: presenter.selectMenuType,
        validators: [
          UtilsValidation.selectRequired(BpMenuText.formMenuType)
        ],
        serverSide: (params) => selectApiTypeMenu(params),
      ),
    );
  }
}