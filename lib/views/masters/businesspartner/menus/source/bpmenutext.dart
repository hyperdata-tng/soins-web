part of datasource;

class BpMenuText {

  static const String title = 'Bisnis Partner Menu';
  static const String subTitle = 'Semua data menu untuk bisnis partner';

  static const String formMenuType = 'Menu';
  static const String formBusinessPartner = 'Bisnis Partner';
}