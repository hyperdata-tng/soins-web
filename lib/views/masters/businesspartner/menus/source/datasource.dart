library datasource;

import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:web/constants/constants.dart';
import 'package:web/presenters/masters/bpmenu_presenter.dart';
import 'package:web/utils/utils.dart';
import 'package:web/helpers/select_api.dart';

part 'bpmenutext.dart';
part 'formsource.dart';