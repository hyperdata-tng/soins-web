library datasource;

import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:web/constants/constants.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/helpers/select_api.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/presenters/masters/menu_presenter.dart';
import 'package:web/utils/utils.dart';

part 'menutext.dart';
part 'menusource.dart';
part 'formsource.dart';