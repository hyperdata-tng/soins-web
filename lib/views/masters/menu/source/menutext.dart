part of datasource;

class MenuText {

  static const String title = 'Menu';
  static const String subTitle = 'Semua data menu';

  static const String formParent = 'Induk';
  static const String formName = 'Nama';
  static const String formIcon = 'icon';
  static const String formRoute = 'Link';
  static const String formSeq = 'Urutan';
  static const String formType = 'Tipe Menu';
}