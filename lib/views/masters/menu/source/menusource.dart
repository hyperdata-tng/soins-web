part of datasource;

class MenuDataSource extends BsDatatableSource {

  bool hasAccessEdit = false;
  ValueChanged<int> onEditListener = (menuid) {};
  bool hasAccessDelete = false;
  ValueChanged<int> onDeleteListener = (menuid) {};

  static List<BsDataColumn> columns = <BsDataColumn>[
    BsDataColumn(label: Text(DBText.tableCellNo), width: 100.0, searchable: false, orderable: false),
    BsDataColumn(label: Text(DBText.formName), columnName: 'menunm'),
    BsDataColumn(label: Text(MenuText.formRoute), columnName: 'seq'),
    BsDataColumn(label: Text(MenuText.formParent), columnName: 'masterid'),
    BsDataColumn(label: Text(MenuText.formType), columnName: 'menutypeid'),
    BsDataColumn(label: Text(MenuText.formSeq), width: 100.0, columnName: 'seq'),
    BsDataColumn(label: Text(DBText.tableCellAction), width: 200.0, searchable: false, orderable: false),
  ];

  List<MenuModel> get menus => List<MenuModel>
      .from(response.data.map((e) => MenuModel.fromJson(e)).toList());

  @override
  BsDataRow getRow(int index) {
    MenuModel menu = menus[index];
    return BsDataRow(
      index: index,
      cells: <BsDataCell>[
        BsDataCell(Text('${controller.start + index + 1}')),
        BsDataCell(Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SelectableText(notNull(menu.menunm)),
            SelectableText(notNull(menu.icon),
              style: TextStyle(
                fontSize: 12.0,
              ),
            )
          ],
        )),
        BsDataCell(Text(notNull(menu.route))),
        BsDataCell(Text(notNull(menu.parent.menunm))),
        BsDataCell(Text(notNull(menu.menutype.typename))),
        BsDataCell(Text(notNull(menu.seq))),
        BsDataCell(Row(
          children: [
            !hasAccessEdit ? Container() : BsButton(
              margin: EdgeInsets.only(right: 5.0),
              prefixIcon: DBIcon.buttonEdit,
              size: BsButtonSize.btnIconSm,
              style: BsButtonStyle.primary,
              onPressed: () => onEditListener(menu.menuid!),
            ),
            !hasAccessDelete ? Container() : BsButton(
              prefixIcon: DBIcon.buttonDelete,
              size: BsButtonSize.btnIconSm,
              style: BsButtonStyle.danger,
              onPressed: () => onDeleteListener(menu.menuid!),
            ),
          ],
        ))
      ]
    );
  }

}