part of datasource;

abstract class MenuFormSource {

  bool isLoading = false;

  BsSelectBoxController selectMaster = BsSelectBoxController();
  BsSelectBoxController selectType = BsSelectBoxController();

  TextEditingController inputName = TextEditingController();
  TextEditingController inputIcon = TextEditingController();
  TextEditingController inputRoute = TextEditingController();
  TextEditingController inputSeq = TextEditingController();

  bool isactive = false;
}

class MenuForm {

  MenuForm({
    required this.context,
    required this.presenter,
  });

  final BuildContext context;

  final MenuPresenter presenter;

  Widget selectType() {
    return BsFormGroup(
      label: Text(MenuText.formType),
      child: BsSelectBox(
        searchable: true,
        disabled: presenter.isLoading,
        selectBoxController: presenter.selectType,
        hintText: DBText.placeholderSelect(MenuText.formType),
        serverSide: (params) => selectApiTypeMenu(params),
        validators: [
          BsSelectValidators.required,
        ],
      ),
    );
  }

  Widget selectParent() {
    return BsFormGroup(
      label: Text(MenuText.formParent),
      child: BsSelectBox(
        searchable: true,
        disabled: presenter.isLoading,
        selectBoxController: presenter.selectMaster,
        hintText: DBText.placeholderSelect(MenuText.formParent),
        serverSide: (params) => selectApiMenu(params),
      ),
    );
  }

  Widget inputName() {
    return BsFormGroup(
      label: Text(MenuText.formName),
      child: BsInput(
        disabled: presenter.isLoading,
        controller: presenter.inputName,
        hintText: DBText.placeholder(MenuText.formName),
        validators: [
          UtilsValidation.inputRequired(MenuText.formName),
          UtilsValidation.maxLength(MenuText.formName, 100),
        ],
      ),
    );
  }

  Widget inputIcon() {
    return BsFormGroup(
      label: Text(MenuText.formIcon),
      child: BsInput(
        disabled: presenter.isLoading,
        controller: presenter.inputIcon,
        hintText: DBText.placeholder(MenuText.formIcon),
        validators: [
          UtilsValidation.maxLength(MenuText.formIcon, 100),
        ],
      ),
    );
  }

  Widget inputRoute() {
    return BsFormGroup(
      label: Text(MenuText.formRoute),
      child: BsInput(
        disabled: presenter.isLoading,
        controller: presenter.inputRoute,
        hintText: DBText.placeholder(MenuText.formRoute),
        validators: [
          UtilsValidation.inputRequired(MenuText.formRoute),
          UtilsValidation.maxLength(MenuText.formRoute, 100),
        ],
      ),
    );
  }

  Widget inputSeq() {
    return BsFormGroup(
      label: Text(MenuText.formSeq),
      child: BsInput(
        disabled: presenter.isLoading,
        controller: presenter.inputSeq,
        hintText: DBText.placeholder(MenuText.formSeq),
        validators: [
          UtilsValidation.inputRequired(MenuText.formSeq),
        ],
        keyboardType: TextInputType.number,
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly
        ],
      ),
    );
  }
}