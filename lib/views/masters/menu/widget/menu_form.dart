import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:web/constants/constants.dart';
import 'package:web/presenters/masters/menu_presenter.dart';
import 'package:web/views/masters/menu/source/datasource.dart';

class MenuFormView extends StatefulWidget {

  const MenuFormView({
    Key? key,
    required this.presenter,
    this.onClose,
    this.onSubmit,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MenuFormViewState();
  }

  final MenuPresenter presenter;

  final VoidCallback? onClose;

  final VoidCallback? onSubmit;
}

class _MenuFormViewState extends State<MenuFormView> {

  GlobalKey<FormState> _formState = GlobalKey<FormState>();

  late MenuForm _menuForm;

  @override
  void initState() {
    _menuForm = MenuForm(
      context: context,
      presenter: widget.presenter
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formState,
      child: BsModal(
        context: context,
        dialog: BsModalDialog(
          child: BsModalContent(
            children: [
              BsModalContainer(title: Text(DBText.formTitle(MenuText.title)), closeButton: true),
              BsModalContainer(
                child: Column(
                  children: [
                    _menuForm.selectParent(),
                    _menuForm.selectType(),
                    _menuForm.inputName(),
                    _menuForm.inputIcon(),
                    _menuForm.inputRoute(),
                    _menuForm.inputSeq(),
                  ],
                ),
              ),
              BsModalContainer(
                  mainAxisAlignment: MainAxisAlignment.end,
                  actions: [
                    BsButton(
                      disabled: widget.presenter.isLoading,
                      margin: EdgeInsets.only(right: 5.0),
                      label: Text(DBText.buttonModalCancel),
                      prefixIcon: DBIcon.buttonModalCancel,
                      style: BsButtonStyle.danger,
                      size: BsButtonSize.btnMd,
                      onPressed: () {
                        Navigator.pop(context);
                        if(widget.onClose != null)
                          widget.onClose!();
                      },
                    ),
                    BsButton(
                      disabled: widget.presenter.isLoading,
                      label: Text(widget.presenter.isLoading ? DBText.buttonProcessing : DBText.buttonModalSave),
                      prefixIcon: DBIcon.buttonModalSave,
                      style: BsButtonStyle.primary,
                      size: BsButtonSize.btnMd,
                      onPressed: () {
                        if(_formState.currentState!.validate() && widget.onSubmit != null)
                          widget.onSubmit!();
                      },
                    ),
                  ]
              ),
            ],
          ),
        ),
      ),
    );
  }
}