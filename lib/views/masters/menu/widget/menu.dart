import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:web/constants/constants.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/presenters/masters/menu_presenter.dart';
import 'package:web/routes/masters/menu_route.dart';
import 'package:web/utils/utils.dart';
import 'package:web/views/masters/menu/source/datasource.dart';
import 'package:web/views/skins/widgets/breadcrumbs.dart';
import 'package:web/views/skins/widgets/wrapper.dart';

class MenuView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MenuViewState();
  }
}

class _MenuViewState extends State<MenuView> implements MenuPresenterContract {

  bool hasAccessCreate = true;

  late MenuPresenter _presenter;

  @override
  void initState() {
    _presenter = MenuPresenter(this);
    hasAccess(MenuRoute.routeKey, DBTypes.menuAccessCreate).then((value) {
      setState(() {
        hasAccessCreate = value;
      });
    });
    super.initState();
  }

  @override
  void updateState() {
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return Wrapper(
      title: MenuText.title,
      subTitle: MenuText.subTitle,
      menuKey: [DBRoute.masterRoute, MenuRoute.menu],
      breadcrumbs: [
        Breadcrumbs.home(context),
        Breadcrumb(label: DBText.master),
        Breadcrumb(label: MenuText.title)
      ],
      child: Container(
        padding: Styles.paddingContent,
        child: BsCard(
          style: Styles.boxCard,
          children: [
            BsCardContainer(title: Text(DBText.dataTitle(MenuText.title)), actions: [
              !hasAccessCreate ? Container() : BsButton(
                label: Text(DBText.buttonAdd),
                prefixIcon: DBIcon.buttonAdd,
                style: BsButtonStyle.primary,
                onPressed: () => _presenter.add(context),
              )
            ]),
            BsCardContainer(
              child: BsDatatable(
                source: _presenter.menuSource,
                columns: MenuDataSource.columns,
                language: WidgetConfig.datatableLanguage,
                serverSide: (params) => _presenter.datatables(context, params),
                customizeLayout: (_) => Styles.datatablesLayout(context, _),
              ),
            )
          ],
        ),
      ),
    );
  }
}