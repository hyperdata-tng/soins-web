import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/material.dart';
import 'package:web/constants/constants.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/presenters/masters/types_presenter.dart';
import 'package:web/routes/masters/type_route.dart';
import 'package:web/utils/utils.dart';
import 'package:web/views/masters/type/source/dartsource.dart';
import 'package:web/views/skins/widgets/breadcrumbs.dart';
import 'package:web/views/skins/widgets/wrapper.dart';

class TypeView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TypeViewState();
  }
}

class _TypeViewState extends State<TypeView> implements TypePresenterContract {

  bool hasAccessCreate = true;
  late TypePresenter _presenter;

  @override
  void initState() {
    _presenter = TypePresenter(this);
    hasAccess(TypeRoute.routeKey, DBTypes.menuAccessCreate).then((value) {
      setState(() {
        hasAccessCreate = value;
      });
    });
    super.initState();
  }

  @override
  void updateState() {
    if(mounted)
      setState(() {
      });
  }

  @override
  Widget build(BuildContext context) {
    return Wrapper(
      title: TypeText.title,
      subTitle: TypeText.subTitle,
      menuKey: [DBRoute.masterRoute, TypeRoute.type],
      breadcrumbs: [
        Breadcrumbs.home(context),
        Breadcrumb(label: DBText.master),
        Breadcrumb(label: TypeText.title)
      ],
      child: Container(
        padding: Styles.paddingContent,
        child: Column(
          children: [
            BsCard(
              style: Styles.boxCard,
              children: [
                BsCardContainer(title: Text(DBText.dataTitle(TypeText.title)), actions: [
                  !hasAccessCreate ? Container() : BsButton(
                    label: Text(DBText.buttonAdd),
                    prefixIcon: DBIcon.buttonAdd,
                    style: BsButtonStyle.primary,
                    onPressed: () => _presenter.add(context),
                  )
                ]),
                BsCardContainer(
                  child: BsDatatable(
                    source: _presenter.typeSource,
                    columns: TypeDataSource.columns,
                    language: WidgetConfig.datatableLanguage,
                    serverSide: (params) => _presenter.datatables(context, params),
                    customizeLayout: (_) => Styles.datatablesLayout(context, _),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}