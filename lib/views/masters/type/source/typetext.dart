part of datasource;

class TypeText {

  static String title = 'Tipe';
  static String subTitle = 'Semua data tipe';

  static String formParent = 'Induk';
  static String formName = 'Nama';
  static String formCode = 'Kode';
  static String formSeq = 'Urutan';
  static String formDescription = 'Deskripsi';
}