part of datasource;

abstract class TypeFormSource {

  bool isLoading = false;

  BsSelectBoxController selectParent = BsSelectBoxController();

  TextEditingController inputCode = TextEditingController();
  TextEditingController inputName = TextEditingController();
  TextEditingController inputSeq = TextEditingController();
  TextEditingController inputDescription = TextEditingController();
}

class TypeForm {

  final BuildContext context;

  final TypePresenter presenter;

  const TypeForm({
    required this.context,
    required this.presenter,
  });

  Widget selectParent() {
    return BsFormGroup(
      label: Text(TypeText.formParent),
      child: BsSelectBox(
        searchable: true,
        disabled: presenter.isLoading,
        hintText: DBText.placeholderSelect(TypeText.formParent),
        selectBoxController: presenter.selectParent,
        validators: [],
        serverSide: (params) => selectApiType(params),
      ),
    );
  }

  Widget inputCode() {
    return BsFormGroup(
      label: Text(TypeText.formCode),
      child: BsInput(
        disabled: presenter.isLoading,
        hintText: DBText.placeholder(TypeText.formCode),
        controller: presenter.inputCode,
        validators: [
          UtilsValidation.maxLength(TypeText.formName, 10),
        ],
      ),
    );
  }

  Widget inputName() {
    return BsFormGroup(
      label: Text(TypeText.formName),
      child: BsInput(
        disabled: presenter.isLoading,
        hintText: DBText.placeholder(TypeText.formName),
        controller: presenter.inputName,
        validators: [
          UtilsValidation.inputRequired(TypeText.formName),
          UtilsValidation.maxLength(TypeText.formName, 100),
        ],
      ),
    );
  }

  Widget inputSeq() {
    return BsFormGroup(
      label: Text(TypeText.formSeq),
      child: BsInput(
        disabled: presenter.isLoading,
        hintText: DBText.placeholder(TypeText.formSeq),
        controller: presenter.inputSeq,
        validators: [
          UtilsValidation.inputRequired(TypeText.formSeq),
        ],
        keyboardType: TextInputType.number,
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly
        ],
      ),
    );
  }

  Widget inputDescription() {
    return BsFormGroup(
      label: Text(TypeText.formDescription),
      child: BsInput(
        disabled: presenter.isLoading,
        hintText: DBText.placeholder(TypeText.formDescription),
        controller: presenter.inputDescription,
        validators: [],
        minLines: 1,
        maxLines: 5,
      ),
    );
  }
}