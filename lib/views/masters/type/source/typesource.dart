part of datasource;

class TypeDataSource extends BsDatatableSource {

  bool hasAccessEdit = false;
  ValueChanged<int> onEditListener = (typeid) {};

  bool hasAccessDelete = false;
  ValueChanged<int> onDeleteListener = (typeid) {};

  static List<BsDataColumn> columns = <BsDataColumn>[
    BsDataColumn(label: Text(DBText.tableCellNo), width: 100, searchable: false, orderable: false),
    BsDataColumn(label: Text(TypeText.formCode), columnName: 'typecd'),
    BsDataColumn(label: Text(TypeText.formName), columnName: 'typename'),
    BsDataColumn(label: Text(TypeText.formParent), columnName: 'masterid'),
    BsDataColumn(label: Text(TypeText.formSeq), width: 100, columnName: 'typeseq'),
    BsDataColumn(label: Text(DBText.tableCellAction), width: 200, searchable: false, orderable: false),
  ];

  List<TypeModel> get types => List<TypeModel>.from(response.data.map((e) {
    return TypeModel.fromJson(e);
  }));

  @override
  BsDataRow getRow(int index) {
    TypeModel type = types[index];

    return BsDataRow(
      index: index,
      cells: <BsDataCell>[
        BsDataCell(SelectableText('${controller.start + index + 1}')),
        BsDataCell(SelectableText(notNull(type.typecd))),
        BsDataCell(SelectableText(notNull(type.typename))),
        BsDataCell(SelectableText(notNull(type.parent.typename))),
        BsDataCell(SelectableText(notNull(type.typeseq))),
        BsDataCell(Row(
          children: [
            !hasAccessEdit ? Container() : BsButton(
              margin: EdgeInsets.only(right: 5.0),
              prefixIcon: DBIcon.buttonEdit,
              size: BsButtonSize.btnIconSm,
              style: BsButtonStyle.primary,
              onPressed: () => onEditListener(type.typeid!),
            ),
            !hasAccessDelete ? Container() : BsButton(
              prefixIcon: DBIcon.buttonDelete,
              size: BsButtonSize.btnIconSm,
              style: BsButtonStyle.danger,
              onPressed: () => onDeleteListener(type.typeid!),
            ),
          ],
        ))
      ]
    );
  }
}