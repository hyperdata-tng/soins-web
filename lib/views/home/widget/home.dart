import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:web/views/skins/widgets/wrapper.dart';

class HomeView extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _HomeViewState();
  }
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Wrapper(
      title: 'Judul Halaman',
      subTitle: 'Subjudul Halaman',
      child: Container(),
    );
  }
}