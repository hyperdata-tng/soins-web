import 'package:web/helpers/helpers.dart';

class BpMenuModel {
  int? bpmenuid;
  int? bpid;
  int? menuid;

  int? createdby;
  DateTime? createddate;
  int? updatedby;
  DateTime? updateddate;
  bool? isactive;

  BpMenuModel({
    this.bpmenuid,
    this.bpid,
    this.menuid,
    this.createdby,
    this.createddate,
    this.updatedby,
    this.updateddate,
    this.isactive,
  });

  factory BpMenuModel.fromJson(Map<String, String> map) {
    return BpMenuModel(
      bpmenuid: parseInt(map['bpmenuid']),
      bpid: parseInt(map['bpid']),
      menuid: parseInt(map['menuid']),
      createdby: parseInt(map['createdby']),
      createddate: parseDate(map['createddate']),
      updatedby: parseInt(map['updatedby']),
      updateddate: parseDate(map['createddate']),
      isactive: parseBool(map['isactive']),
    );
  }
}