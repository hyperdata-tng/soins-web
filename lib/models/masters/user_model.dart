import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/userdt_model.dart';

class UserModel {

  int? userid;
  String? username;
  String? userpassword;
  String? userfullname;
  String? useremail;
  String? userphone;
  String? userdeviceid;
  String? token;

  DateTime? createddate;
  int? createdby;
  DateTime? updateddate;
  int? updatedby;
  bool? isactive;

  List _userdetails;
  Map<String, dynamic>? _userdetail;

  UserModel({
    this.userid,
    this.username,
    this.userpassword,
    this.userfullname,
    this.useremail,
    this.userphone,
    this.userdeviceid,
    this.token,
    this.createddate,
    this.createdby,
    this.updateddate,
    this.updatedby,
    this.isactive,
    Map<String, dynamic>? userdetail,
    List userdetails = const [],
    List accessmenus = const [],
  }) : _userdetails = userdetails,
    _userdetail = userdetail;

  factory UserModel.fromJson(Map<String, dynamic> map) {
    return UserModel(
      userid: parseInt(map['userid']),
      username: parseString(map['username']),
      userpassword: parseString(map['userpassword']),
      userfullname: parseString(map['userfullname']),
      useremail: parseString(map['useremail']),
      userphone: parseString(map['userphone']),
      userdeviceid: parseString(map['userdeviceid']),
      token: parseString(map['token']),
      createddate: parseDate(map['createddate']),
      createdby: parseInt(map['createdby']),
      updateddate: parseDate(map['updateddate']),
      updatedby: parseInt(map['updatedby']),
      isactive: parseBool(map['isactive']),
      userdetails: map['userdetails'],
      userdetail: map['userdetail'],
    );
  }

  static Map<String, dynamic> toJson(UserModel user) {
    Map<String, dynamic> json = Map<String, dynamic>();
    json.addAll({
      'userid': notNull(user.userid),
      'username': notNull(user.username),
      'userfullname': notNull(user.userfullname),
      'useremail': notNull(user.useremail),
      'userphone': notNull(user.userphone),
      'userdeviceid': notNull(user.userdeviceid),
    });

    return json;
  }

  List<UserDetailModel> get userdetails {
    if(_userdetails.length == 0)
      return [];

    return List<UserDetailModel>.from(_userdetails
        .map((e) => UserDetailModel.fromJson(e)));
  }

  UserDetailModel get userdetail {
    if(_userdetail == null)
      return UserDetailModel();

    return UserDetailModel.fromJson(_userdetail!);
  }

}