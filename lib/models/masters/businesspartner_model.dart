import 'package:web/helpers/helpers.dart';

class BusinessPartnerModel {

  int? bpid;
  String? bpname;
  int? bptypeid;
  String? bppicname;
  String? bpemail;
  String? bpphone;

  int? createdby;
  DateTime? createddate;
  int? updatedby;
  DateTime? updateddate;
  bool? isactive;

  BusinessPartnerModel({
    this.bpid,
    this.bpname,
    this.bptypeid,
    this.bppicname,
    this.bpemail,
    this.bpphone,
    this.createdby,
    this.createddate,
    this.updatedby,
    this.updateddate,
    this.isactive
  });

  factory BusinessPartnerModel.fromJson(Map<String, dynamic> map) {
    return BusinessPartnerModel(
      bpid: parseInt(map['bpid']),
      bpname: parseString(map['bpname']),
      bptypeid: parseInt(map['bptypeid']),
      bppicname: parseString(map['bppicname']),
      bpemail: parseString(map['bpemail']),
      bpphone: parseString(map['bpphone']),
      createdby: parseInt(map['createdby']),
      createddate: parseDate(map['createddate']),
      updatedby: parseInt(map['updatedby']),
      updateddate: parseDate(map['updateddate']),
      isactive: parseBool(map['isactive']),
    );
  }

  static Map<String, dynamic> toJson(BusinessPartnerModel businessPartner) {
    Map<String, dynamic> json = Map<String, dynamic>();
    json.addAll({
      'bpid': notNull(businessPartner.bpid),
      'bpname': notNull(businessPartner.bpname),
      'bptypeid': notNull(businessPartner.bptypeid),
      'bppicname': notNull(businessPartner.bppicname),
      'bpemail': notNull(businessPartner.bpemail),
      'bpphone': notNull(businessPartner.bpphone),
    });
    return json;
  }
}