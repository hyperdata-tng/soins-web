import 'package:web/helpers/helpers.dart';

class TypeModel {

  int? typeid;
  String? typecd;
  String? typename;
  int? typeseq;
  int? masterid;
  String? descriptions;

  Map<String, dynamic>? _parent;

  int? createdby;
  DateTime? createddate;
  int? updatedby;
  DateTime? updateddate;
  bool? isactive;

  TypeModel({
    this.typeid,
    this.typecd,
    this.typename,
    this.typeseq,
    this.masterid,
    this.descriptions,
    this.createdby,
    this.createddate,
    this.updatedby,
    this.updateddate,
    this.isactive,
    Map<String, dynamic>? parent,
  }) : _parent = parent;

  factory TypeModel.fromJson(Map<String, dynamic> map) {
    return TypeModel(
      typeid: parseInt(map['typeid']),
      typecd: parseString(map['typecd']),
      typename: parseString(map['typename']),
      typeseq: parseInt(map['typeseq']),
      masterid: parseInt(map['masterid']),
      descriptions: parseString(map['descriptions']),
      createdby: parseInt(map['createdby']),
      createddate: parseDate(map['createddate']),
      updatedby: parseInt(map['updatedby']),
      updateddate: parseDate(map['updateddate']),
      isactive: parseBool(map['isactive']),
      parent: map['parent'],
    );
  }

  static Map<String, dynamic> toJson(TypeModel type) {
    Map<String, dynamic> json = Map<String, dynamic>();
    json.addAll({
      'typeid': notNull(type.typeid),
      'typecd': notNull(type.typecd),
      'typename': notNull(type.typename),
      'typeseq': notNull(type.typeseq),
      'descriptions': notNull(type.descriptions),
    });
    return json;
  }

  TypeModel get parent {
    if(_parent == null)
      return TypeModel();

    return TypeModel.fromJson(_parent!);
  }
}