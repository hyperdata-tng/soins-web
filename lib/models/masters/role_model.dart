import 'dart:convert';

import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/businesspartner_model.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/models/masters/type_model.dart';

class RoleModel {

  int? roleid;
  int? usertypeid;
  int? menuid;
  int? bpid;
  int? accessid;
  
  bool? checked;

  Map<String, dynamic>? _usertype;
  Map<String, dynamic>? _menu;
  Map<String, dynamic>? _businesspartner;
  Map<String, dynamic>? _access;

  int? createdby;
  DateTime? createddate;
  int? updatedby;
  DateTime? updateddate;
  bool? isactive;

  RoleModel({
    this.roleid,
    this.usertypeid,
    this.menuid,
    this.bpid,
    this.accessid,
    this.createdby,
    this.createddate,
    this.updatedby,
    this.updateddate,
    this.isactive,
    this.checked,
    Map<String, dynamic>? usertype,
    Map<String, dynamic>? menu,
    Map<String, dynamic>? businesspartner,
    Map<String, dynamic>? access,
  }) : _usertype = usertype,
    _menu = menu,
    _businesspartner = businesspartner,
    _access = access;

  factory RoleModel.fromJson(Map<String, dynamic> map) {
    return RoleModel(
      roleid: parseInt(map['roleid']),
      usertypeid: parseInt(map['usertypeid']),
      menuid: parseInt(map['menuid']),
      bpid: parseInt(map['bpid']),
      checked: parseBool(map['checked']),
      accessid: parseInt(map['accessid']),
      createdby: parseInt(map['createdby']),
      createddate: parseDate(map['createddate']),
      updatedby: parseInt(map['updatedby']),
      updateddate: parseDate(map['updateddate']),
      isactive: parseBool(map['isactive']),
      usertype: map['usertype'],
      menu: map['menu'],
      businesspartner: map['businesspartner'],
      access: map['access'],
    );
  }

  static List<RoleModel> fromJsonList(List<String> json) {
    if(json.length == 0)
      return [];

    return List<RoleModel>.from(json.map((e) {
      return RoleModel.fromJson(jsonDecode(e));
    }));
  }

  static List jsonList(List<RoleModel> roles, {bool jsonFormat = true}) {
    List json = List.from([]);
    roles.forEach((role) {
      if(jsonFormat)
        json.add(jsonEncode(RoleModel.toJson(role)));
      
      else 
        json.add(RoleModel.toJson(role));
    });
    return json;
  }

  static Map<String, dynamic> toJson(RoleModel role) {
    Map<String, dynamic> json = Map<String, dynamic>();
    json.addAll({
      'checked': notNull(role.checked),
      'roleid': notNull(role.roleid),
      'usertypeid': notNull(role.usertypeid),
      'menuid': notNull(role.menuid),
      'bpid': notNull(role.bpid),
      'accessid': notNull(role.accessid),
      'access': TypeModel.toJson(role.access)
    });
    return json;
  }

  TypeModel get usertype {
    if(_usertype == null)
      return TypeModel();

    return TypeModel.fromJson(_usertype!);
  }

  MenuModel get menu {
    if(_menu == null)
      return MenuModel();

    return MenuModel.fromJson(_menu!);
  }

  BusinessPartnerModel get businesspartner {
    if(_businesspartner == null)
      return BusinessPartnerModel();

    return BusinessPartnerModel.fromJson(_businesspartner!);
  }

  TypeModel get access {
    if(_access == null)
      return TypeModel();

    return TypeModel.fromJson(_access!);
  }
}