import 'dart:convert';

import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/role_model.dart';
import 'package:web/models/masters/type_model.dart';

class MenuModel {
  int? menuid;
  int? masterid;
  int? menutypeid;
  String? menunm;
  String? icon;
  String? route;
  int? seq;

  Map<String, dynamic>? _menutype;
  Map<String, dynamic>? _parent;
  List? _accesses;
  
  bool? checked;
  int? bpmenuid;

  int? createdby;
  DateTime? createddate;
  int? updatedby;
  DateTime? updateddate;
  bool? isactive;

  MenuModel({
    this.bpmenuid,
    this.menuid,
    this.masterid,
    this.menutypeid,
    this.menunm,
    this.icon,
    this.route,
    this.seq,
    this.createdby,
    this.createddate,
    this.updatedby,
    this.updateddate,
    this.isactive,
    this.checked,
    Map<String, dynamic>? menutype,
    Map<String, dynamic>? parent,
    List? accesses = const [],
  }) : _menutype = menutype,
    _parent = parent,
    _accesses = accesses;

  factory MenuModel.fromJson(Map<String, dynamic> map) {
    return MenuModel(
      bpmenuid: parseInt(map['bpmenuid']),
      menuid: parseInt(map['menuid']),
      masterid: parseInt(map['masterid']),
      menutypeid: parseInt(map['menutypeid']),
      menunm: parseString(map['menunm']),
      icon: parseString(map['icon']),
      route: parseString(map['route']),
      seq: parseInt(map['seq']),
      checked: parseBool(map['checked']),
      createdby: parseInt(map['createdby']),
      createddate: parseDate(map['createddate']),
      updatedby: parseInt(map['updatedby']),
      updateddate: parseDate(map['updateddate']),
      isactive: parseBool(map['isactive']),
      menutype: map['menutype'],
      parent: map['parent'],
      accesses: map['accesses'],
    );
  }

  static List<MenuModel> fromJsonList(List<String> json) {
    if(json.length == 0)
      return [];

    return List<MenuModel>.from(json.map((e) {
      return MenuModel.fromJson(jsonDecode(e));
    }));
  }

  static List jsonList(List<MenuModel> menus, {bool jsonFormat = true}) {
    List json = List.from([]);
    menus.forEach((menu) {
      if(jsonFormat)
        json.add(jsonEncode(MenuModel.toJson(menu)));

      else
        json.add(MenuModel.toJson(menu));
    });
    return json;
  }

  static Map<String, dynamic> toJson(MenuModel menu) {
    Map<String, dynamic> json = Map<String, dynamic>();
    json.addAll({
      'checked': notNull(menu.checked),
      'bpmenuid': notNull(menu.bpmenuid),
      'menuid': notNull(menu.menuid),
      'masterid': notNull(menu.masterid),
      'menutypeid': notNull(menu.menutypeid),
      'menunm': notNull(menu.menunm),
      'icon': notNull(menu.icon),
      'route': notNull(menu.route),
      'seq': notNull(menu.seq),
      'menutype': TypeModel.toJson(menu.menutype),
      'accesses': RoleModel.jsonList(menu.accesses, jsonFormat: false),
    });
    return json;
  }

  TypeModel get menutype {
    if(_menutype == null)
      return TypeModel();

    return TypeModel.fromJson(_menutype!);
  }

  MenuModel get parent {
    if(_parent == null)
      return MenuModel();

    return MenuModel.fromJson(_parent!);
  }

  List<RoleModel> get accesses {
    if(_accesses == null || (_accesses != null && _accesses!.length == 0))
      return [];

    return List<RoleModel>.from(_accesses!
        .map((e) => RoleModel.fromJson(e)));
  }

  set accesses(dynamic accesses) => _accesses = accesses;
}