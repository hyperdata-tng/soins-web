import 'dart:convert';

import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/businesspartner_model.dart';
import 'package:web/models/masters/type_model.dart';

class UserDetailModel {

  int? userdtid;
  int? userid;
  int? usertypeid;
  int? bpid;
  String? referalcode;
  int? relationid;

  int? createdby;
  DateTime? createddate;
  int? updatedby;
  DateTime? updateddate;
  bool? isactive;

  Map<String, dynamic>? _businesspartner;
  Map<String, dynamic>? _usertype;

  UserDetailModel({
    this.userdtid,
    this.userid,
    this.usertypeid,
    this.bpid,
    this.referalcode,
    this.relationid,
    this.createdby,
    this.createddate,
    this.updatedby,
    this.updateddate,
    this.isactive,
    Map<String, dynamic>? businesspartner,
    Map<String, dynamic>? usertype,
  }) : _businesspartner = businesspartner,
    _usertype = usertype;

  static UserDetailModel fromJson(Map<String, dynamic> map) {
    return UserDetailModel(
      userdtid: parseInt(map['userdtid']),
      userid: parseInt(map['userid']),
      usertypeid: parseInt(map['usertypeid']),
      bpid: parseInt(map['bpid']),
      referalcode: parseString(map['referalcode']),
      relationid: parseInt(map['relationid']),
      createdby: parseInt(map['createdby']),
      createddate: parseDate(map['createddate']),
      updatedby: parseInt(map['updatedby']),
      updateddate: parseDate(map['updateddate']),
      isactive: parseBool(map['isactive']),
      businesspartner: map['businesspartner'],
      usertype: map['usertype'],
    );
  }

  static List<UserDetailModel> fromJsonList(List<String> maps) {
    if(maps.length == 0)
      return [];

    return List<UserDetailModel>.from(maps.map((e) {
      return UserDetailModel.fromJson(jsonDecode(e));
    }));
  }

  static Map<String, dynamic> toJson(UserDetailModel userDetail) {
    Map<String, dynamic> json = Map<String, dynamic>();
    json.addAll({
      'userdtid': notNull(userDetail.userdtid),
      'userid': notNull(userDetail.userid),
      'usertypeid': notNull(userDetail.usertypeid),
      'bpid': notNull(userDetail.bpid),
      'referalcode': notNull(userDetail.referalcode),
      'relationid': notNull(userDetail.relationid),
      'usertype': TypeModel.toJson(userDetail.usertype),
      'businesspartner': BusinessPartnerModel.toJson(userDetail.businesspartner),
    });
    return json;
  }

  static List<String> jsonList(List<UserDetailModel> userDetails) {
    List<String> json = List<String>.from([]);
    userDetails.forEach((userDetail) {
      json.add(jsonEncode(UserDetailModel.toJson(userDetail)));
    });
    return json;
  }

  TypeModel get usertype {
    if(_usertype == null)
      return TypeModel();

    return TypeModel.fromJson(_usertype!);
  }

  BusinessPartnerModel get businesspartner {
    if(_businesspartner == null)
      return BusinessPartnerModel();

    return BusinessPartnerModel.fromJson(_businesspartner!);
  }
}