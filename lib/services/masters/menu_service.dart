import 'dart:io';

import 'package:http_repository/http_repository.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/utils/config.dart';
import 'package:web/utils/session.dart';

class MenuService extends RepositoryCRUD {

  @override
  String get api => '${Config.api}/menu';

  @override
  Future<String> get credentials async => 'Bearer ${await SessionUtils.get(SessionCode.token)}';

  Future<Response> access(int usertypeid, int bpid) async {
    return Repository.get('$api/access',
      body: {
        'usertypeid': notNull(usertypeid),
        'bpid': notNull(bpid)
      },
      headers: {
        HttpHeaders.authorizationHeader: await credentials
      }
    );
  }
}