import 'dart:io';

import 'package:http_repository/http_repository.dart';
import 'package:web/utils/config.dart';
import 'package:web/utils/session.dart';

class TypeService extends RepositoryCRUD {

  @override
  String get api => '${Config.api}/types';

  @override
  Future<String> get credentials async => 'Bearer ${await SessionUtils.get(SessionCode.token)}';

  Future<Response> fetch({Map<String, String> params = const {}}) async {
    return Repository.get('$api/fetch', body: params, headers: {
      HttpHeaders.authorizationHeader: await credentials,
    });
  }
}