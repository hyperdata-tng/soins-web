import 'dart:io';

import 'package:http_repository/http_repository.dart';
import 'package:web/utils/config.dart';
import 'package:web/utils/session.dart';

class BpMenuService extends RepositoryCRUD {

  @override
  String get api => '${Config.api}/bpmenu';

  @override
  Future<String> get credentials async => 'Bearer ${await SessionUtils.get(SessionCode.token)}';

  Future<Response> load(String bpid, String menutypeid) async {
    return Repository.get('$api/load', body: {'bpid': bpid, 'menutypeid': menutypeid},
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${await SessionUtils.get(SessionCode.token)}'
      }
    );
  }

  Future<Response> save(Map<String, String> body) async {
    return await Repository.post('$api/save', body: body, headers: {
      HttpHeaders.authorizationHeader: await credentials,
    });
  }
}