import 'package:http_repository/http_repository.dart';
import 'package:web/utils/config.dart';
import 'package:web/utils/session.dart';

class BusinessPartnerService extends RepositoryCRUD {

  @override
  String get api => '${Config.api}/businesspartner';

  @override
  Future<String> get credentials async => 'Bearer ${await SessionUtils.get(SessionCode.token)}';
}