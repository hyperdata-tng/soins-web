import 'dart:io';

import 'package:http_repository/http_repository.dart';
import 'package:web/utils/config.dart';
import 'package:web/utils/session.dart';

class AppService {

  Future<Response> update(String userid,String bpid, String usertypeid) async {
    return Repository.get('${Config.api}/update',
      body: {
        'userid': userid,
        'bpid': bpid,
        'usertypeid': usertypeid,
      },
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${await SessionUtils.get(SessionCode.token)}',
      }
    );
  }
}