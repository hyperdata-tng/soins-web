import 'dart:io';

import 'package:http_repository/http_repository.dart';
import 'package:web/utils/config.dart';
import 'package:web/utils/session.dart';

class PrevilegeService extends RepositoryCRUD {

  @override
  String get api => '${Config.api}/role';

  @override
  Future<String> get credentials async => 'Bearer ${await SessionUtils.get(SessionCode.token)}';

  Future<Response> access(String usertypeid, String bpid, String menutypeid) async {
    return await Repository.get('$api/access', body: {
      'usertypeid': usertypeid,
      'bpid': bpid,
      'menutypeid': menutypeid,
    }, headers: {
      HttpHeaders.authorizationHeader: await credentials,
    });
  }

  Future<Response> save(Map<String, String> body) async {
    return await Repository.post('$api/access', body: body, headers: {
      HttpHeaders.authorizationHeader: await credentials,
    });
  }
}