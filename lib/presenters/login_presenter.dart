import 'package:flutter/cupertino.dart';
import 'package:web/models/masters/user_model.dart';
import 'package:web/routes.dart';
import 'package:web/routes/home_route.dart';
import 'package:web/services/auth_service.dart';
import 'package:web/utils/session.dart';
import 'package:web/utils/utils.dart';

abstract class LoginPresenterContract extends ViewContract {}

class LoginPresenter {

  final LoginPresenterContract viewContract;

  LoginPresenter(this.viewContract);

  AuthService authService = AuthService();

  bool isLoading = false;

  TextEditingController inputUsername = TextEditingController();
  TextEditingController inputPassword = TextEditingController();

  String? errorMessage;

  void setLoading(bool loading) {
    isLoading = loading;
    viewContract.updateState();

    if(loading)
      errorMessage = null;
  }

  void login(BuildContext context) {
    try {
      setLoading(true);
      authService.login(username: inputUsername.text, password: inputPassword.text).then((value) {
        if(value.result!) {
          UserModel user = UserModel.fromJson(value.data);
          SessionUtils.setSession(SessionUtils(
            userid: user.userid!,
            fullname: user.userfullname!,
            token: user.token!,
            userdetail: user.userdetail,
            userdetails: user.userdetails,
          ));
          SessionUtils.setExpired(DateTime.parse(value.data['expired']));
          Routes.redirect(context, HomeRoute.home);
          setLoading(false);
        }

        else {
          errorMessage = value.message;
          setLoading(false);
        }
      });
    } catch(e) {
      setLoading(false);
    }
  }
}