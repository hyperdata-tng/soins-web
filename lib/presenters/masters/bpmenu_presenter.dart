import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/services/masters/bpmenu_service.dart';
import 'package:web/utils/session.dart';
import 'package:web/utils/utils.dart';
import 'package:web/views/masters/businesspartner/menus/source/datasource.dart';

abstract class BpMenuPresenterContract extends ViewContract {}

class BpMenuPresenter extends BpMenuFormSource {

  final BpMenuPresenterContract viewContract;

  BpMenuPresenter(this.viewContract);

  BpMenuService bpMenuService = BpMenuService();

  Map<int, MenuModel> menus = Map<int, MenuModel>();

  void setLoading(bool loading) {
    isLoading = loading;
    viewContract.updateState();
  }

  void load(BuildContext context) {
    try {
      setLoading(true);
      menus.clear();
      bpMenuService.load(selectBp.getSelectedAsString(), selectMenuType.getSelectedAsString()).then((res) {
        if(res.result!) {
          res.data.forEach((e) {
            MenuModel menu = MenuModel.fromJson(e);
            menus.addAll({menu.menuid!: menu});
          });
        }

        setLoading(false);
      });
    } catch(e) {
      setLoading(false);
    }
  }

  Future save(BuildContext context) async {
    try {
      setLoading(true);
      SessionUtils session = await SessionUtils.getInstance();

      List<MenuModel> sendmenus = List<MenuModel>.from([]);
      menus.forEach((key, value) {
        sendmenus.add(value);
      });

      bpMenuService.save({
        'userid': notNull(session.userid),
        'bpid': selectBp.getSelectedAsString(),
        'menus': jsonEncode(MenuModel.jsonList(sendmenus, jsonFormat: false))
      }).then((res) {
        if(res.result!) {
          load(context);
        }

        setLoading(false);
      });
    } catch(e, trace) {
      print(e);
      print(trace);
      setLoading(false);
    }
  }
}