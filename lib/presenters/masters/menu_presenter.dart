import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:web/constants/constants.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/helpers/widget/confirm_dialog.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/routes/masters/menu_route.dart';
import 'package:web/services/masters/menu_service.dart';
import 'package:web/utils/session.dart';
import 'package:web/utils/utils.dart';
import 'package:web/views/masters/menu/source/datasource.dart';
import 'package:web/views/masters/menu/widget/menu_form.dart';

abstract class MenuPresenterContract extends ViewContract {}

class MenuPresenter extends MenuFormSource {

  GlobalKey<State> _formView = GlobalKey<State>();

  final MenuPresenterContract viewContract;

  MenuPresenter(this.viewContract);

  MenuService menuService = MenuService();
  MenuModel menuModel = MenuModel();

  MenuDataSource menuSource = MenuDataSource();

  void setLoading(bool loading) {
    isLoading = loading;
    viewContract.updateState();

    if(_formView.currentState != null)
      _formView.currentState!.setState(() {
      });
  }

  Future<Map<String, String>> getDatas() async {
    SessionUtils sesion = await SessionUtils.getInstance();
    return {
      'menutypeid': selectType.getSelectedAsString(),
      'masterid': notNull(selectMaster.getSelectedAsString(), nullValue: '0'),
      'menunm': inputName.text,
      'icon': inputIcon.text,
      'route': inputRoute.text,
      'seq': inputSeq.text,
      'createdby': notNull(sesion.userid),
      'updatedby': notNull(sesion.userid),
    };
  }

  void setDatas() {

    if(menuModel.menutype.typeid != null)
      selectType.setSelected(BsSelectBoxOption(value: menuModel.menutype.typeid, text: Text(notNull(menuModel.menutype.typename))));

    if(menuModel.parent.menuid != null)
      selectMaster.setSelected(
        BsSelectBoxOption(value: menuModel.parent.menuid, text: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(notNull(menuModel.parent.menunm)),
            menuModel.parent.parent.menuid == null ? Container() : Text(notNull(menuModel.parent.parent.menunm),
              style: TextStyle(fontSize: 12.0),
            )
          ],
        ))
      );

    inputName.text = notNull(menuModel.menunm);
    inputIcon.text = notNull(menuModel.icon);
    inputRoute.text = notNull(menuModel.route);
    inputSeq.text = notNull(menuModel.seq);
  }

  void resetDatas() {
    selectType.clear();
    selectMaster.clear();
    inputName.clear();
    inputIcon.clear();
    inputRoute.clear();
    inputSeq.clear();
  }

  Future datatables(BuildContext context, Map<String, String> params) async {
    try {
      return await menuService.datatables(params).then((value) async {
        if(value.result!) {
          menuSource.response = BsDatatableResponse.createFromJson(value.data);
          menuSource.hasAccessEdit = await hasAccess(MenuRoute.routeKey, DBTypes.menuAccessUpdate);
          menuSource.onEditListener = (menuid) => edit(context, menuid);

          menuSource.hasAccessDelete = await hasAccess(MenuRoute.routeKey, DBTypes.menuAccessDelete);
          menuSource.onDeleteListener = (menuid) => destroy(context, menuid);
        }

        setLoading(false);
      });
    } catch(e) {
      setLoading(false);
    }
  }

  void add(BuildContext context) {
    try {
      setLoading(false);
      resetDatas();
      showDialog(
        context: context,
        builder: (context) => MenuFormView(
          key: _formView,
          presenter: this,
          onSubmit: () => store(context),
        ),
      );
    } catch(e) {
      setLoading(false);
    }
  }

  void store(BuildContext context) {
    try {
      setLoading(true);

      getDatas().then((datas) {
        menuService.store(datas).then((res) {
          if(res.result!) {
            Navigator.pop(context);
            menuSource.controller.reload();
          }

          setLoading(false);
        });
      });
    } catch(e) {
      setLoading(false);
    }
  }

  void edit(BuildContext context, int menuid) {
    try {
      setLoading(false);
      resetDatas();

      showDialog(
        context: context,
        builder: (context) => MenuFormView(
          key: _formView,
          presenter: this,
          onSubmit: () => update(context, menuid),
        ),
      );

      menuService.show(menuid).then((res) {
        if(res.result!) {
          menuModel = MenuModel.fromJson(res.data);
          setDatas();
        } else Navigator.pop(context);

        setLoading(false);
      });
    } catch(e) {
      setLoading(false);
    }
  }

  void update(BuildContext context, int menuid) {
    try {
      setLoading(true);

      getDatas().then((datas) {
        menuService.update(menuid, datas).then((value) {
          if(value.result!) {
            Navigator.pop(context);
            menuSource.controller.reload();
          }

          setLoading(false);
        });
      });
    } catch(e) {
      setLoading(false);
    }
  }

  void destroy(BuildContext context, int menuid) {
    showDialog(
      context: context,
      builder: (context) => ConfirmDialog(
        onPressed: (_, value) {
          if(value == ConfirmDialogOption.YES_OPTION) {
            try {
              _.setDisabled(true);
              menuService.delete(menuid).then((res) {
                if(res.result!) {
                  Navigator.pop(context);
                  menuSource.controller.reload();
                }

                _.setDisabled(false);
              });
            } catch(e) {
              _.setDisabled(false);
            }
          }
        },
      ),
    );
  }
}