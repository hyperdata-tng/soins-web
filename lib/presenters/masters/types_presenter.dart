import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:web/constants/constants.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/helpers/widget/confirm_dialog.dart';
import 'package:web/models/masters/type_model.dart';
import 'package:web/routes/masters/type_route.dart';
import 'package:web/services/masters/type_service.dart';
import 'package:web/utils/session.dart';
import 'package:web/utils/utils.dart';
import 'package:web/views/masters/type/source/dartsource.dart';
import 'package:web/views/masters/type/widget/type_form.dart';

abstract class TypePresenterContract extends ViewContract {}

class TypePresenter extends TypeFormSource {

  GlobalKey<State> _formView = GlobalKey<State>();

  final TypePresenterContract viewContract;

  TypePresenter(this.viewContract);

  TypeModel typeModel = TypeModel();
  TypeService typeService = TypeService();

  TypeDataSource typeSource = TypeDataSource();

  void setLoading(bool loading) {
    isLoading = loading;
    viewContract.updateState();

    if(_formView.currentState != null)
      _formView.currentState!.setState(() {
      });
  }

  Future<Map<String, String>> getDatas() async {
    SessionUtils sesion = await SessionUtils.getInstance();
    return {
      'masterid': notNull(selectParent.getSelectedAsString(), nullValue: '0'),
      'typename': inputName.text,
      'typecd': inputCode.text,
      'typeseq': inputSeq.text,
      'descriptions': inputDescription.text,
      'createdby': notNull(sesion.userid),
      'updatedby': notNull(sesion.userid)
    };
  }

  void setDatas() {
    if(typeModel.parent.typeid != null)
      selectParent.setSelected(BsSelectBoxOption(value: typeModel.parent.typeid, text: Text(notNull(typeModel.parent.typename))));

    inputName.text = notNull(typeModel.typename);
    inputCode.text = notNull(typeModel.typecd);
    inputSeq.text = notNull(typeModel.typeseq);
    inputDescription.text = notNull(typeModel.descriptions);
  }

  void resetDatas() {
    selectParent.clear();
    inputName.clear();
    inputCode.clear();
    inputSeq.clear();
    inputDescription.clear();
  }

  Future datatables(BuildContext context, Map<String, String> params) async {
    try {
      return await typeService.datatables(params).then((res) async {
        if(res.result!) {
          typeSource.response = BsDatatableResponse.createFromJson(res.data);
          typeSource.hasAccessEdit = await hasAccess(TypeRoute.routeKey, DBTypes.menuAccessUpdate);
          typeSource.onEditListener = (typeid) => edit(context, typeid);
          typeSource.hasAccessDelete = await hasAccess(TypeRoute.routeKey, DBTypes.menuAccessDelete);
          typeSource.onDeleteListener = (typeid) => delete(context, typeid);
        }
      });
    } catch(e) {
      setLoading(false);
    }
  }

  void add(BuildContext context) {
    try {
      setLoading(false);
      resetDatas();

      showDialog(
        context: context,
        builder: (context) => TypeFormView(
          key: _formView,
          presenter: this,
          onSubmit: () => store(context),
        ),
      );
    } catch(e) {}
  }

  void store(BuildContext context) {
    try {
      setLoading(false);
      getDatas().then((datas) {
        typeService.store(datas).then((res) {
          if(res.result!) {
            Navigator.pop(context);
            typeSource.controller.reload();
          }

          setLoading(false);
        });
      });
    } catch(e) {
      setLoading(false);
    }
  }

  void edit(BuildContext context, int typeid) {
    try {
      setLoading(false);
      resetDatas();

      showDialog(
        context: context,
        builder: (context) => TypeFormView(
          key: _formView,
          presenter: this,
          onSubmit: () => update(context, typeid),
        ),
      );

      typeService.show(typeid).then((res) {
        if(res.result!) {
          typeModel = TypeModel.fromJson(res.data);
          setDatas();
        }

        setLoading(false);
      });
    } catch(e) {
      setLoading(false);
    }
  }

  void update(BuildContext context, int typeid) {
    try {
      setLoading(true);
      getDatas().then((datas) {
        typeService.update(typeid, datas).then((res) {
          if(res.result!) {
            Navigator.pop(context);
            typeSource.controller.reload();
          }

          setLoading(false);
        });
      });
    } catch(e) {
      setLoading(false);
    }
  }

  void delete(BuildContext context, int typeid) {
    showDialog(
      context: context,
      builder: (context) => ConfirmDialog(
        onPressed: (_, value) {
          try {
            _.setDisabled(true);
            typeService.delete(typeid).then((res) {
              if(res.result!) {
                Navigator.pop(context);
                typeSource.controller.reload();
              };

              _.setDisabled(false);
            });
          } catch(e) {
            _.setDisabled(false);
          }
        },
      ),
    );
  }
}