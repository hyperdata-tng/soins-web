import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/models/masters/type_model.dart';
import 'package:web/services/settings/previlege_service.dart';
import 'package:web/utils/session.dart';
import 'package:web/utils/utils.dart';
import 'package:web/views/settings/privilege/source/datasource.dart';

abstract class PrevilegePresenterContract extends ViewContract {}

class PrevilegePresenter extends PrevilegeFormSource {

  final bool developer;
  final PrevilegePresenterContract viewContract;

  PrevilegePresenter(this.viewContract, this.developer);

  PrevilegeService previlegeService = PrevilegeService();

  Map<int, MenuModel> menus = Map<int, MenuModel>();
  List<TypeModel> accesses = List<TypeModel>.from([]);

  void setLoading(bool loading) {
    isLoading = loading;
    viewContract.updateState();
  }

  void load(BuildContext context) async {
    try {
      SessionUtils session = await SessionUtils.getInstance();

      setLoading(true);
      menus.clear();
      previlegeService.access(
        selectUserType.getSelectedAsString(),
        developer ? selectBp.getSelectedAsString() : notNull(session.userdetail.bpid),
        selectMenuType.getSelectedAsString()
      ).then((res) {
        if(res.result!) {
          res.data['previleges'].forEach((e) {
            MenuModel menu = MenuModel.fromJson(e);
            menus.addAll({menu.menuid!: menu});
          });
          accesses = List<TypeModel>.from(res.data['accesses'].map((e) {
            return TypeModel.fromJson(e);
          }));
        }

        setLoading(false);
      });
    } catch(e) {}
  }

  Future save(BuildContext context) async {
    try {
      setLoading(true);

      List<MenuModel> sendmenus = List<MenuModel>.from([]);
      menus.forEach((key, value) {
        sendmenus.add(value);
      });

      SessionUtils session = await SessionUtils.getInstance();
      previlegeService.save({
        'usertypeid': selectUserType.getSelectedAsString(),
        'userid': notNull(session.userid),
        'bpid': developer ? selectBp.getSelectedAsString() : notNull(session.userdetail.bpid),
        'menus': jsonEncode(MenuModel.jsonList(sendmenus, jsonFormat: false)),
      }).then((res) {
        if(res.result!) {
          load(context);
        }
        setLoading(false);
      });
    } catch(e) {
      setLoading(false);
    }
  }
}