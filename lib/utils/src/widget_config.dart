part of utils;

class WidgetConfig {

  static BsDatatableLanguage datatableLanguage = BsDatatableLanguage(
    nextPagination: 'Selanjutnya',
    previousPagination: 'Sebelumnya',
    searchLabel: 'Pencarian',
    perPageLabel: 'Per Halaman',
    hintTextSearch: 'Cari ,,,',
    lastPagination: 'Terakhir',
    firstPagination: 'Pertama',
    information: 'Tampil __START__ sampai __END__ dari __FILTERED__ data',
    informationFiltered: 'disaring dari __DATA__ total data'
  );
}