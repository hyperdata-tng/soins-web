part of utils;

class Styles {

  static const BoxShadow shadowRegular = BoxShadow(
    color: Color(0xffd9d9d9),
    spreadRadius: 4.0,
    blurRadius: 12.0
  );

  static const BsCardStyle boxCard = BsCardStyle(
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(5.0)),
      boxShadow: [
        Styles.shadowRegular
      ]
    )
  );

  static const BsInputSize outlineBottomMd = BsInputSize(
    fontSize: 14.0,
    padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 14.0, bottom: 14.0),
    marginTop: 14.0,
    marginLeft: 5.0,
  );

  static Color colorPrimary = Colors.blueAccent;
  static Color colorPrimaryHover = Color(0xff366ecc);
  static Color colorPrimarySplash = Color(0xff22457f);

  static EdgeInsets paddingContent = EdgeInsets.only(left: 15.0, right: 15.0);

  static const BsButtonStyle roundedPrimary = BsButtonStyle(
      color: Colors.white,
      backgroundColor: BsColor.primary,
      borderRadius: BorderRadius.all(Radius.circular(50.0)));

  static Widget datatablesLayout(BuildContext context, _) {
    return Column(
      children: [
        BsRow(
          children: [
            BsCol(
              visibility: BsVisibility.hiddenSm,
              margin: EdgeInsets.only(bottom: 15.0),
              sizes: ColScreen.inDevice(mobile: Col.col_12, tablet: Col.col_6, desktop: Col.col_8),
              child: _.pageLength(),
            ),
            BsCol(
              margin: EdgeInsets.only(bottom: 15.0),
              sizes: ColScreen.inDevice(mobile: Col.col_12, tablet: Col.col_6, desktop: Col.col_4),
              child: Row(
                mainAxisAlignment: BreakPoint.isMobile(context) ? MainAxisAlignment.start : MainAxisAlignment.end,
                children: [
                  _.searchForm(),
                ],
              ),
            )
          ],
        ),
        BsRow(
          children: [
            BsCol(
              margin: EdgeInsets.only(bottom: 15.0),
              sizes: ColScreen.all(Col.col_12),
              child: _.table(),
            )
          ],
        ),
        BsRow(
          children: [
            BsCol(
              margin: EdgeInsets.only(bottom: 15.0),
              sizes: ColScreen.all(Col.col_12),
              child: _.footer(),
            )
          ],
        )
      ],
    );
  }
}