library session;

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http_repository/http_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web/constants/constants.dart';
import 'package:web/helpers/helpers.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/models/masters/userdt_model.dart';
import 'package:web/routes.dart';
import 'package:web/routes/auth/auth_route.dart';
import 'package:web/routes/home_route.dart';
import 'package:web/services/masters/type_service.dart';
import 'package:web/views/errors/page_not_found.dart';

part 'session/session_splash_screen.dart';
part 'session/session_utils.dart';
part 'session/session_view.dart';