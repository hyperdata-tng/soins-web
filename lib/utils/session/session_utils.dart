part of session;

class SessionCode {

  static const String userid = 'tNsjQ';
  static const String fullname = 'Cv6O1';
  static const String token = 'sBN13s';
  static const String userdetail = 'NsaS8b';
  static const String userdetails = 'Pe237C';
  static const String accessmenus = 's8c13D';
  static const String exipredtoken = 's82Xh2';
}

class SessionUtils {

  int? userid;
  String? fullname;
  String? token;
  DateTime? expiredtoken;
  UserDetailModel userdetail;
  List<UserDetailModel> userdetails;

  List<MenuModel> accessmenus;

  SessionUtils({
    this.userid,
    this.fullname,
    this.token,
    this.expiredtoken,
    required this.userdetail,
    this.userdetails = const [],
    this.accessmenus = const [],
  });

  static Future<SessionUtils> getInstance() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    DateTime? expiredToken;
    if(prefs.getInt(SessionCode.exipredtoken) != null)
      expiredToken = DateTime.fromMicrosecondsSinceEpoch(prefs.getInt(SessionCode.exipredtoken)!);

    UserDetailModel userdetail = UserDetailModel();
    if(prefs.getString(SessionCode.userdetail) != null)
      userdetail = UserDetailModel.fromJson(jsonDecode(prefs.getString(SessionCode.userdetail)!));

    List<String> userdetails = [];
    if(prefs.getStringList(SessionCode.userdetails) != null
        && prefs.getStringList(SessionCode.userdetails)!.length > 0)
      userdetails = prefs.getStringList(SessionCode.userdetails)!;

    List<String> accessmenus = [];
    if(prefs.getStringList(SessionCode.accessmenus) != null
        && prefs.getStringList(SessionCode.accessmenus)!.length > 0)
      accessmenus = prefs.getStringList(SessionCode.accessmenus)!;

    return SessionUtils(
      userid: prefs.getInt(SessionCode.userid),
      fullname: prefs.getString(SessionCode.fullname),
      token: prefs.getString(SessionCode.token),
      expiredtoken: expiredToken,
      userdetail: userdetail,
      userdetails: UserDetailModel.fromJsonList(userdetails),
      accessmenus: MenuModel.fromJsonList(accessmenus),
    );
  }

  static Future get(String code) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get(code);
  }

  static void updateUserDetail(UserDetailModel userdetail) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(SessionCode.userdetail, jsonEncode(UserDetailModel.toJson(userdetail)));
  }

  static Future setAccessMenus(List<MenuModel> accessmenus) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> _accessmenus = List<String>.from([]);
    accessmenus.forEach((accessmenu) {
      _accessmenus.add(jsonEncode(MenuModel.toJson(accessmenu)));
    });

    prefs.setStringList(SessionCode.accessmenus, _accessmenus);
  }

  static Future setExpired(DateTime expireddate) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(SessionCode.exipredtoken, expireddate.microsecondsSinceEpoch);
  }

  static Future setSession(SessionUtils session) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(SessionCode.userid, session.userid!);
    prefs.setString(SessionCode.fullname, session.fullname!);
    prefs.setString(SessionCode.token, session.token!);
    prefs.setString(SessionCode.userdetail, jsonEncode(UserDetailModel.toJson(session.userdetail)));
    prefs.setStringList(SessionCode.userdetails, UserDetailModel.jsonList(session.userdetails));
  }

  static Future remove(String code) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(code);
  }

  static Future removeSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(SessionCode.userid);
    prefs.remove(SessionCode.fullname);
    prefs.remove(SessionCode.token);
    prefs.remove(SessionCode.userdetail);
    prefs.remove(SessionCode.userdetails);
    prefs.remove(SessionCode.accessmenus);
    prefs.remove(SessionCode.exipredtoken);
  }
}
