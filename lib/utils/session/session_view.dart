part of session;

class Session extends StatefulWidget {

  const Session({
    required this.builder,
    this.routeKey,
    this.accessCode,
    this.guest = false,
  });

  @override
  State<StatefulWidget> createState() {
    return _SessionState();
  }

  final bool guest;

  final String? routeKey;

  final String? accessCode;

  final Future Function(BuildContext context) builder;
}

class _SessionState extends State<Session> {

  Widget? _child;

  bool _isLogged = false;
  bool _processing = true;

  @override
  void initState() {
    _checkSession();
    super.initState();
  }

  void _checkSession() async {
    SessionUtils session = await SessionUtils.getInstance();

    bool pIsLogged = false;
    bool pHasAccess = false;

    /* SESSION CHECK */

    /* Jika terdapat session dan bukan halaman guest maka login valid */
    if(session.userid != null
        && session.fullname != null
        && session.token != null
        && session.expiredtoken != null
        && session.userdetail.userdtid != null
        && session.userdetails.length > 0) {

      /* Jika bukan guest maka tampil halaman yang di request */
      if(widget.guest == false)
        pIsLogged = true;

      /* Jika guest maka redirect ke halaman awal terlebih dahulu */
      else
        Routes.redirect(context, HomeRoute.home);
    }

    /* Jika halaman guest maka login valid dan akan diarahkan ke halaman yg direquest */
    else if(widget.guest) {
      pIsLogged = true;
    }

    /* Jika selain 2 kondisi diatas maka login tidak valid dan diarahkan ke halaman login*/
    else {
      SessionUtils.removeSession().then((value) {
        Routes.redirect(context, AuthRoute.login);
      });
    }

    if(widget.guest == false) {
      if(session.expiredtoken != null && session.expiredtoken!.compareTo(DateTime.now()) < 1)
        SessionUtils.removeSession().then((value) {
          Routes.redirect(context, AuthRoute.login);
        });

      try {
        Response response = await TypeService().fetch();
        if (response.result!) {
          DBTypes.types = response.data;
        }
      } catch (e) {}
    }

    if(widget.routeKey != null && widget.accessCode != null)
      pHasAccess = await hasAccess(widget.routeKey!, widget.accessCode!);

    else pHasAccess = true;

    if(pIsLogged && pHasAccess)
      _child = await widget.builder(context);

    setState(() {
      _isLogged = pIsLogged && pHasAccess;
      _processing = false;
    });
  }


  @override
  Widget build(BuildContext context) {
    return _processing ? SessionSplashScreen()
        : _isLogged ? _child! : PageNotFoundView();
  }
}