import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/material.dart';
import 'package:http_repository/http_repository.dart';
import 'package:web/constants/constants.dart';
import 'package:web/models/masters/menu_model.dart';
import 'package:web/services/masters/businesspartner_service.dart';
import 'package:web/services/masters/menu_service.dart';
import 'package:web/services/masters/type_service.dart';

Future<BsSelectBoxResponse> selectApiType(Map<String, String> params) async {
  Response response = await TypeService().select(params);
  if(response.result!)
    return BsSelectBoxResponse.createFromJson(response.data);

  return BsSelectBoxResponse(options: []);
}

Future<BsSelectBoxResponse> selectApiTypeMenu(Map<String, String> params) async {
  params.addAll({'typecd': DBTypes.menu});
  return selectApiType(params);
}

Future<BsSelectBoxResponse> selectApiUserType(Map<String, String> params) async {
  params.addAll({'typecd': DBTypes.role});
  return selectApiType(params);
}

Future<BsSelectBoxResponse> selectApiMenu(Map<String, String> params) async {
  Response response = await MenuService().select(params);
  if(response.result!)
    return BsSelectBoxResponse.createFromJson(response.data,
      value: (data) {
        MenuModel menuModel = MenuModel.fromJson(data);
        return menuModel.menuid;
      },
      renderText: (data) {
        MenuModel menuModel = MenuModel.fromJson(data);
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('${menuModel.menunm}'),
            menuModel.parent.menuid == null ? Container() : Text('${menuModel.parent.menunm}',
              style: TextStyle(
                fontSize: 10.0
              ),
            )
          ],
        );
      },
    );

  return BsSelectBoxResponse(options: []);
}

Future<BsSelectBoxResponse> selectApiBusinessPartner(Map<String, String> params) async {
  Response response = await BusinessPartnerService().select(params);
  if(response.result!)
    return BsSelectBoxResponse.createFromJson(response.data);

  return BsSelectBoxResponse(options: []);
}