import 'package:fluro/fluro.dart';
import 'package:web/routes.dart';
import 'package:web/utils/session.dart';
import 'package:web/views/login/widget/login.dart';

class AuthRoute {

  static const String routeKey = 'login';

  static const String login = '/$routeKey';

  static void register(Routes routes) {
    routes.defined(login, handler: Handler(
      handlerFunc: (context, parameters) => Session(
        guest: true,
        builder: (context) async => LoginView(),
      ),
    ));
  }
}