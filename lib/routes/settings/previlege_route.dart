import 'package:fluro/fluro.dart';
import 'package:web/constants/constants.dart';
import 'package:web/routes.dart';
import 'package:web/utils/session.dart';
import 'package:web/views/settings/privilege/widget/previlege.dart';

class PrevilegeRoute {

  static const String routeKey = 'previlege';
  static const String previlege = '/$routeKey';

  static void register(Routes routes) {
    routes.defined(previlege, handler: Handler(
      handlerFunc: (context, parameters) => Session(
        routeKey: routeKey,
        accessCode: DBTypes.menuAccessView,
        builder: (context) async {
          return PrevilegeView();
        },
      ),
    ));
  }
}