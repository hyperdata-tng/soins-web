import 'package:fluro/fluro.dart';
import 'package:web/constants/constants.dart';
import 'package:web/routes.dart';
import 'package:web/utils/session.dart';
import 'package:web/views/settings/privilege/widget/previlege.dart';

class PrevilegesRoute {

  static const String routeKey = 'previleges';
  static const String previleges = '/$routeKey';

  static void register(Routes routes) {
    routes.defined(previleges, handler: Handler(
      handlerFunc: (context, parameters) => Session(
        routeKey: routeKey,
        accessCode: DBTypes.menuAccessView,
        builder: (context) async {
          return PrevilegeView(developer: true);
        },
      ),
    ));
  }
}