import 'package:fluro/fluro.dart';
import 'package:web/routes.dart';
import 'package:web/utils/session.dart';
import 'package:web/views/home/widget/home.dart';

class HomeRoute {

  static String routeKey = '';

  static String home = '/';

  static void register(Routes routes) {
    routes.defined(home, handler: Handler(
      handlerFunc: (context, parameters) => Session(
        builder: (context) async => HomeView(),
      ),
    ));
  }
}