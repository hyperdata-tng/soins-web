import 'package:fluro/fluro.dart';
import 'package:web/constants/constants.dart';
import 'package:web/routes.dart';
import 'package:web/utils/session.dart';
import 'package:web/views/masters/businesspartner/menus/widget/bpmenu.dart';

class BpMenuRoute {

  static const String routeKey = 'bpmenu';

  static const String bpmenu = '/$routeKey';

  static void register(Routes routes) {
    routes.defined(bpmenu, handler: Handler(
      handlerFunc: (context, parameters) => Session(
        routeKey: routeKey,
        accessCode: DBTypes.menuAccessView,
        builder: (context) async {
          return BpMenuView();
        },
      ),
    ));
  }
}