import 'package:fluro/fluro.dart';
import 'package:web/constants/constants.dart';
import 'package:web/routes.dart';
import 'package:web/utils/session.dart';
import 'package:web/views/masters/type/widget/type.dart';

class TypeRoute {

  static const String routeKey = 'types';
  static const String type = '/$routeKey';

  static void register(Routes routes) {
    routes.defined(type, handler: Handler(
      handlerFunc: (context, parameters) => Session(
        routeKey: routeKey,
        accessCode: DBTypes.menuAccessView,
        builder: (context) async {
          return TypeView();
        },
      ),
    ));
  }
}