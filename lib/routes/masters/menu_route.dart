import 'package:fluro/fluro.dart';
import 'package:web/constants/constants.dart';
import 'package:web/routes.dart';
import 'package:web/utils/session.dart';
import 'package:web/views/masters/menu/widget/menu.dart';

class MenuRoute {

  static const String routeKey = 'menu';

  static const String menu = '/$routeKey';

  static void register(Routes routes) {
    routes.defined(menu, handler: Handler(
      handlerFunc: (context, parameters) => Session(
        routeKey: routeKey,
        accessCode: DBTypes.menuAccessView,
        builder: (context) async => MenuView(),
      ),
    ));
  }
}