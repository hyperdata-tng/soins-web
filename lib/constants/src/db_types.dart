part of constants;

class DBTypes {

  static Map<String, dynamic> types = Map<String, dynamic>();

  static String menu = notNull(types['menu']);

  static String role = notNull(types['role']);

  static String menuAccessView = notNull(types['menuAccessView']);
  static String menuAccessCreate = notNull(types['menuAccessCreate']);
  static String menuAccessUpdate = notNull(types['menuAccessUpdate']);
  static String menuAccessDelete = notNull(types['menuAccessDelete']);
}