part of constants;

class DBRoute {

  static const String masterRoute = '/master';
  static const String settingRoute = '/settings';
  static const String businesspartnerRoute = '/businesspartner';
}