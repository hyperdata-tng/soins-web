import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:web/routes/auth/auth_route.dart';
import 'package:web/routes/home_route.dart';
import 'package:web/routes/masters/bpmenu_route.dart';
import 'package:web/routes/masters/menu_route.dart';
import 'package:web/routes/masters/type_route.dart';
import 'package:web/routes/settings/previlege_route.dart';
import 'package:web/routes/settings/previleges_route.dart';
import 'package:web/views/errors/page_not_found.dart';

class Routes {

  static final router = FluroRouter();

  static void redirect(BuildContext context, String path, {
    RouteSettings? routeSettings
  }) => router.navigateTo(context, path, transition: TransitionType.fadeIn, routeSettings: routeSettings);

  void defined(String routePath, {
    required Handler? handler,
    TransitionType transitionType = TransitionType.fadeIn
  }) => router.define(routePath, handler: handler, transitionType: transitionType);

  Route<dynamic>? generator(RouteSettings routeSettings) => router.generator(routeSettings);

  Routes() {
    AuthRoute.register(this);

    HomeRoute.register(this);

    MenuRoute.register(this);
    TypeRoute.register(this);
    BpMenuRoute.register(this);

    PrevilegeRoute.register(this);
    PrevilegesRoute.register(this);

    router.notFoundHandler = Handler(
      handlerFunc: (context, parameters) => PageNotFoundView(),
    );
  }
}